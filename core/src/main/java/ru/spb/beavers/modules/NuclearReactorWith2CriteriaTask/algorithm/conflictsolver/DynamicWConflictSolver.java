package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.algorithm.conflictsolver;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.Challenger;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.ListenerHandler;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.IStepLogger;

public class DynamicWConflictSolver extends ConflictSolver {
    private double beta_;
    private double r_min_;
    private double r_max_;
    private ListenerHandler<IStepLogger> listener_handler_;

    private double costFunction(double value){
        return beta_ * (1.0 - Math.pow(beta_,-(value - r_min_)/(r_max_ - r_min_))) / (beta_ - 1.0);
    }

    public DynamicWConflictSolver(double beta,double r_min,double r_max){
        beta_ = beta;
        r_min_ = r_min;
        r_max_ = r_max;
    }
    public DynamicWConflictSolver(double beta){
        this(beta,-1.0,1.0);
    }
    public DynamicWConflictSolver(){
        this(1.0,-1.0,1.0);
    }


    public void setBeta (double beta) {
        beta_ = beta;
    }

    public void setRMin (double r_min) {
        r_min_ = r_min;
    }

    public void setRMax (double r_max) {
        r_max_ = r_max;
    }
    @Override
    final public Challenger chose (Challenger challanger_a, Challenger challanger_b) {
        logAction("Для каждого из возможный решений расчитаем стоимость по формуле : ri = vi + w( ri, " + beta_ + ", " + r_min_ + ", " + r_max_ + ")*si.<br>" );
        logAction("Для первого решения стоимость равна : " + (challanger_a.getValue().getV() + costFunction(challanger_a.getValue().getV()) * challanger_a.getValue().getN()) + "<br>");
        logAction("Для второго решения стоимость равна : " + (challanger_b.getValue().getV() + costFunction(challanger_b.getValue().getV()) * challanger_b.getValue().getN()) + "<br>");
        if (challanger_a.getValue().getV() + costFunction(challanger_a.getValue().getV()) * challanger_a.getValue().getN() >=
                challanger_b.getValue().getV() + costFunction(challanger_b.getValue().getV()) * challanger_b.getValue().getN() )
        {
            logAction("Т.к стоимость первого решения больше стоимости второго решения, то выбираем первое решение.");
            return challanger_a;
        } else {
            logAction("Т.к стоимость второго решения больше стоимости первого решения, то выбираем второго решение.");
            return challanger_b;
        }
    }

    public String getTypeString(){
        return "Переменный вес как функция W(ri,beta,rmin,rmax)";
    }
}