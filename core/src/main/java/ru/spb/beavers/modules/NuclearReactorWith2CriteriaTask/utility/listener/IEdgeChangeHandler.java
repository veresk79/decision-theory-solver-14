package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.Edge;

public interface IEdgeChangeHandler {
    public void setPrefered(Edge edge);
    public void valueChanged(Edge edge);
    public void probabilityChanged(Edge edge);
}
