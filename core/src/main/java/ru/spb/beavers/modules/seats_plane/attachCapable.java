package ru.spb.beavers.modules.seats_plane;

import com.sun.javafx.beans.annotations.NonNull;

import javax.swing.*;

/**
 * Created by Никита on 12.04.15.
 */
public interface attachCapable {

    public void attachToExPanel(@NonNull JPanel exPanel);
}
