package ru.spb.beavers.modules.seats_plane;

import com.sun.javafx.beans.annotations.NonNull;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;
import ru.spb.beavers.modules.ITaskModule;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Никита on 12.04.15.
 */
public class creatComponentAsist {

    static public void  setPicture(@NonNull JPanel pan, String picResource)
    {
        JLabel picLabel;
        try{
            BufferedImage myPicture = ImageIO.read(ITaskModule.class.getResource(TaskCommon.PIC_PATH + picResource));
            picLabel = new JLabel(new ImageIcon(myPicture));
        }
        catch (Exception ex)
        {
            System.out.println("LOG:: creatComponentAsist img exception");
            picLabel =  new JLabel("Image resource was there");
            ex.printStackTrace();

        }
        picLabel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        pan.add(picLabel);
    }

    static public JPanel customPanel( String textLabel, Component... compList)
    {
        JPanel custPanel = new JPanel();
        custPanel.add(new JLabel(textLabel));
        JPanel compPanel = new JPanel();
        compPanel.setLayout(new BoxLayout(compPanel, BoxLayout.Y_AXIS));
        for( Component comp : compList)
        {
            compPanel.add(comp);
        }
        custPanel.add(compPanel);
        custPanel.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        return custPanel;
    }

    static public JLabel formula(String latexReg)
    {
        String latex = latexReg;

        // create a formula
        TeXFormula formula = new TeXFormula(latex);

        // render the formla to an icon of the same size as the formula.
        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);

        // insert a border
        icon.setInsets(new Insets(5, 5, 5, 5));

        // now create an actual image of the rendered equation
        BufferedImage image = new BufferedImage(icon.getIconWidth(),
                icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2 = image.createGraphics();
        g2.setColor(Color.white);
        g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());

        JLabel jl = new JLabel();
        jl.setForeground(new Color(0, 0, 0));
        icon.paintIcon(jl, g2, 0, 0);
        // at this point the image is created, you could also save it with ImageIO

        JLabel formulaIcon = new JLabel(new ImageIcon(image));

        return  formulaIcon;
    }

}
