package ru.spb.beavers.modules;

import ru.spb.beavers.modules.seats_plane.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


/**
 * Developed by N. Mishnev / M. Kallavus on 10.04.2015.
 *
 * Task module to solve exercise 3.1.1
 */
public class SeatsPlane implements ITaskModule  {

    InputWidget inWidget = new InputWidget();
    Description description =  new Description( TaskCommon.DESC_TITLE, TaskCommon.DESC_MESSAGE, TaskCommon.PIC_AIRPLANE);
    Solution solution = new Solution();
    Solver solver = new Solver();

    public SeatsPlane()
    {

    }

    @Override
    public String getTitle()
    {
        return TaskCommon.GLOBAL_TITLE;
    }

    public void initDescriptionPanel(JPanel panel)
    {
        description.attachToExPanel(panel);
    }


    public void initSolutionPanel(JPanel panel)
    {
        solution.attachToExPanel(panel);
    }


    public void initInputPanel(JPanel panel)
    {
        inWidget.attachToExPanel(panel);
    }

    public void initExamplePanel(JPanel panel)
    {
        solver.initTerms(inWidget.getSprosType(),inWidget.getSprosValue(), inWidget.getPlaceNum(), inWidget.getPriceVf(), inWidget.getPriceVd());
        solver.resolve();
        solver.attachToExPanel(panel);
    }


    public ActionListener getPressSaveListener() throws IllegalArgumentException
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FileNameExtensionFilter filter = new FileNameExtensionFilter("*.br9","*.*");
                JFileChooser fc = new JFileChooser();
                fc.setFileFilter(filter);
                if ( fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION ) {
                    try ( FileWriter fw = new FileWriter(fc.getSelectedFile()) ) {
                        fw.write(inWidget.toString());
                    }
                    catch ( IOException ex ) {
                        ex.printStackTrace();
                    }
                }
            }
        };
    }


    public ActionListener getPressLoadListener() throws IllegalArgumentException
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser fileopen = new JFileChooser();

                int ret = fileopen.showDialog(null, "Открыть файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileopen.getSelectedFile();
                    try{
                        Scanner scanner = new Scanner(file);
                        StringBuilder fileContents = new StringBuilder((int)file.length());
                        String lineSeparator = System.getProperty("line.separator");
                        while(scanner.hasNextLine()) {
                            fileContents.append(scanner.nextLine() + lineSeparator);
                            }
                        scanner.close();
                        System.out.println("FILE LOAD: "+fileContents.toString());
                        inWidget.loadData(fileContents.toString());
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }
            }
        };
    }


    public ActionListener getDefaultValuesListener() throws IllegalArgumentException
    {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                inWidget.setDefaultFieldValues();
            }
        };
    }


}
