package ru.spb.beavers.modules;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.DescriptionPanel;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.ExamplePanel;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.InputPanel;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.SolutionPanel;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.algorithm.conflictsolver.ConflictSolver;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.Edge;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.Node;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.NodeType;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.INewSolverHandler;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NuclearReactorWith2CriteriaTaskModule implements ITaskModule {

    private DescriptionPanel dp;
    private SolutionPanel sp;
    private InputPanel ip;
    private ExamplePanel ep;
    private Node root_node;
    private boolean ep_initialized;

    public NuclearReactorWith2CriteriaTaskModule(){
        dp = null;
        sp = null;
        ip = null;
        ep = null;


        ep_initialized = false;
        root_node = getTree();
    }

    private Node getTree(){
        Node node1  = new Node("1", NodeType.DECISION);
        Node node2  = new Node("2", NodeType.LOTTERY);
        Node node31 = new Node("3.1", NodeType.DECISION);
        Node node32 = new Node("3.2", NodeType.DECISION);
        Node node33 = new Node("3.3", NodeType.DECISION);
        Node node41 = new Node("4.1", NodeType.LOTTERY);
        Node node42 = new Node("4.2", NodeType.LOTTERY);
        Node node43 = new Node("4.3", NodeType.LOTTERY);
        Node node51 = new Node("5.1", NodeType.LOTTERY);
        Node node52 = new Node("5.2", NodeType.LOTTERY);

        Node node61 = new Node("6.1", NodeType.ACCIDENT);
        Node node62 = new Node("6.2", NodeType.ACCIDENT);
        Node node63 = new Node("6.3", NodeType.ACCIDENT);
        Node node64 = new Node("6.4", NodeType.ACCIDENT);
        Node node65 = new Node("6.5", NodeType.ACCIDENT);
        Node node66 = new Node("6.6", NodeType.ACCIDENT);
        Node node67 = new Node("6.7", NodeType.ACCIDENT);
        Node node68 = new Node("6.8", NodeType.ACCIDENT);
        Node node69 = new Node("6.9",   NodeType.ACCIDENT);
        Node node610 = new Node("6.10", NodeType.ACCIDENT);
        Node node611 = new Node("6.11", NodeType.ACCIDENT);
        Node node612 = new Node("6.12", NodeType.ACCIDENT);

        Edge edge2 = new Edge("1-2", "T","DT");
        Edge edge31 = new Edge("1-3.1", "N","DN");
        Edge edge32 = new Edge("2-3.2","p","Dp");
        Edge edge33 = new Edge("2-3.3","n","Dn");

        Edge edge41 = new Edge("3.1-4.1", "C1","DC1");
        Edge edge42 = new Edge("3.2-4.2", "C2","DC2");
        Edge edge43 = new Edge("3.2-5.1", "A1","DA1");
        Edge edge51 = new Edge("3.3-4.3", "C3","DC3");
        Edge edge52 = new Edge("3.3-5.2", "A2","DA2");

        Edge edge61 = new Edge("4.1-6.1", "s1","Ds1");
        Edge edge62 = new Edge("4.1-6.2", "f1","Df1");
        Edge edge63 = new Edge("4.2-6.3", "s2","Ds2");
        Edge edge64 = new Edge("4.2-6.4","f2","Df2");
        Edge edge65 = new Edge("5.1-6.5", "s3","Ds3");
        Edge edge66 = new Edge("5.1-6.6", "m3","Dm3");
        Edge edge67 = new Edge("5.1-6.7", "l3","Dl3");
        Edge edge68 = new Edge("4.3-6.8", "s4","Ds4");
        Edge edge69 = new Edge("4.3-6.9", "f4","Df4");
        Edge edge610 = new Edge("5.2-6.10", "s5","Ds5");
        Edge edge611 = new Edge("5.2-6.11", "m5","Dm5");
        Edge edge612 = new Edge("5.2-6.12", "l5","Dl5");

        node2.setEdgeToParent(edge2);
        node31.setEdgeToParent(edge31);
        node32.setEdgeToParent(edge32);
        node33.setEdgeToParent(edge33);
        node41.setEdgeToParent(edge41);
        node42.setEdgeToParent(edge42);
        node43.setEdgeToParent(edge43);
        node51.setEdgeToParent(edge51);
        node52.setEdgeToParent(edge52);
        node61.setEdgeToParent(edge61);
        node62.setEdgeToParent(edge62);
        node63.setEdgeToParent(edge63);
        node64.setEdgeToParent(edge64);
        node65.setEdgeToParent(edge65);
        node66.setEdgeToParent(edge66);
        node67.setEdgeToParent(edge67);
        node68.setEdgeToParent(edge68);
        node69.setEdgeToParent(edge69);
        node610.setEdgeToParent(edge610);
        node611.setEdgeToParent(edge611);
        node612.setEdgeToParent(edge612);

        node1.addChild(node2);
        node1.addChild(node31);

        node2.addChild(node32);
        node2.addChild(node33);

        node31.addChild(node41);

        node32.addChild(node42);
        node32.addChild(node51);

        node33.addChild(node43);
        node33.addChild(node52);

        node41.addChild(node61);
        node41.addChild(node62);

        node42.addChild(node63);
        node42.addChild(node64);

        node51.addChild(node65);
        node51.addChild(node66);
        node51.addChild(node67);

        node43.addChild(node68);
        node43.addChild(node69);

        node52.addChild(node610);
        node52.addChild(node611);
        node52.addChild(node612);

        return  node1;
    }

    @Override
    public String getTitle(){
        return "1.5.2 Задача о ядерном\n реакторе с 2мя критериями";
    }

    @Override
    public void initDescriptionPanel(JPanel panel){
        if(dp == null) dp = new DescriptionPanel(panel);
        dp.fillPanel(panel);
    }

    @Override
    public void initSolutionPanel(JPanel panel){
        if(sp == null)sp = new SolutionPanel(panel);
        sp.fillPanel(panel);
    }

    @Override
    public void initInputPanel(JPanel panel) {
        if( ip == null) ip = new InputPanel(panel, root_node);
        ip.fillPanel(panel);
        if (ep != null) {
            ip.addComponent(new INewSolverHandler() {
                @Override
                public void newSolver(ConflictSolver solver) {
                    ep.setSolver(solver);
                }
            });
        }
    }

    @Override
    public void initExamplePanel(JPanel panel){
        if(ep == null) ep = new ExamplePanel(panel, root_node);
        ep.fillPanel(panel);
        if (!ep_initialized) {
            ep.setSolver(ip.getSolver());
            ip.addComponent(new INewSolverHandler() {
                @Override
                public void newSolver(ConflictSolver solver) {
                    ep.setSolver(solver);
                }
            });
            ep_initialized = true;
        }
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException{
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                FileFilter filter = new FileNameExtensionFilter("JTable input data","jtid");
                fileChooser.setFileFilter(filter);
                if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                    ip.writeToFile(fileChooser.getSelectedFile());
                }
            }
        };
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException{
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    fileChooser.getSelectedFile();
                    ip.readFromFile(fileChooser.getSelectedFile());
                }
            }
        };
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException{
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ip.setDefualtTableValues();
            }
        };
    }
}
