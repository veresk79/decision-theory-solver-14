package ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.RenderContext;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import edu.uci.ics.jung.visualization.transform.shape.GraphicsDecorator;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

/**
 * Created by Sasha on 09.04.2015.
 */
public class RenderDiagram implements Renderer.Vertex<VertexS, String> {
    @Override public void paintVertex(RenderContext<VertexS, String> rc,
                                      Layout<VertexS, String> layout, VertexS vertex) {
        GraphicsDecorator graphicsContext = rc.getGraphicsContext();
        Point2D center = layout.transform(vertex);
        Shape shape = null;
        Color color2 = null;
        Color color1 = new Color(0,0,0);
        if(vertex.shape.equals("Square"))
        {
            shape = new Rectangle((int)center.getX()-10, (int)center.getY()-10, 20, 20);
            color2 = new Color(255, 0, 5);
        }
        else if(vertex.shape.equals("Rhomb"))
        {
            Polygon p = new Polygon();
            p.addPoint((int)center.getX(), (int)center.getY()-12);
            p.addPoint((int)center.getX()-12, (int)center.getY());
            p.addPoint((int)center.getX(), (int)center.getY()+12);
            p.addPoint((int)center.getX()+12, (int)center.getY());
            shape = p;
            color2 = new Color(134, 225, 0);
        }
        else if(vertex.shape.equals("Circle"))
        {
            shape = new Ellipse2D.Double(center.getX()-10, center.getY()-10, 20, 20);
            color2 = new Color(200, 200, 0);
        }
        else if(vertex.shape.equals("Dot"))
        {
            shape = new Ellipse2D.Double(center.getX()-3, center.getY()-3, 6, 6);
            color2 = new Color(255, 255, 255);
        }
        else if(vertex.shape.equals("Dotm"))
        {
            shape = new Ellipse2D.Double(center.getX()-3, center.getY()-3, 0, 0);
            color2 = new Color(0, 0, 0);
        }
        graphicsContext.setPaint(color2);
        graphicsContext.fill(shape);
        graphicsContext.setPaint(color1);
        graphicsContext.draw(shape);
    }
}
