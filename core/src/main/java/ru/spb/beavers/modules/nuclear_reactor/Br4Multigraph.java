package ru.spb.beavers.modules.nuclear_reactor;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.util.EdgeType;

/**
 * Created by bazleks on 11.04.2015.
 * This graph is used for task about Nuclear Reactor
 */
public class Br4Multigraph {

    // ----------------------------- ДАННЫЕ ДЕРЕВА РЕШЕНИЙ ---------------------------------------------------
    // --------------------- ВЕРШИНЫ ------------------------
    public static final Vertex v1 = new Vertex(1, true, "Решение о проверке");
    public static final Vertex v13 = new Vertex(3, true, "Решение о постройке реактора (без проверки)");
    public static final Vertex v2 = new Vertex(2, false, "Результат проверки");
    public static final Vertex v23p = new Vertex(3, true, "Решение о постройке реактора (проверка-успех)");
    public static final Vertex v23n = new Vertex(3, true, "Решение о постройке реактора (проверка-авария)");
    public static final Vertex v34 = new Vertex(4, false, "Результат работы обычного реактора (без проверки)");
    public static final Vertex v34p = new Vertex(4, false, "Результат работы обычного реактора (проверка-успех)");
    public static final Vertex v35p = new Vertex(5, false, "Результат работы передового реактора (проверка-успех)");
    public static final Vertex v34n = new Vertex(4, false, "Результат работы обычного реактора (проверка-авария)");
    public static final Vertex v35n = new Vertex(5, false, "Результат работы передового реактора (проверка-авария)");
    public static final Vertex v46s = new Vertex(6, false, "Успех обычного реактора (без проверки)");
    public static final Vertex v46f = new Vertex(6, false, "Авария обычного реактора (без проверки)");
    public static final Vertex v46ps = new Vertex(6, false, "Успех обычного реактора (проверка-успех)");
    public static final Vertex v46pf = new Vertex(6, false, "Авария обычного реактора (проверка-успех)");
    public static final Vertex v56ps = new Vertex(6, false, "Успех передового реактора (проверка-успех)");
    public static final Vertex v56pm = new Vertex(6, false, "Небольшая авария передового реактора (проверка-успех)");
    public static final Vertex v56pl = new Vertex(6, false, "Крупная авария передового реактора (проверка-успех)");
    public static final Vertex v46ns = new Vertex(6, false, "Успех обычного реактора (проверка-авария)");
    public static final Vertex v46nf = new Vertex(6, false, "Авария обычного реактора (проверка-авария)");
    public static final Vertex v56ns = new Vertex(6, false, "Успех передового реактора (проверка-авария)");
    public static final Vertex v56nm = new Vertex(6, false, "Небольная авария передового реактора (проверка-авария)");
    public static final Vertex v56nl = new Vertex(6, false, "Крупная авария передового реактора (проверка-авария)");
    // --------------------- ГРАНИ ----------------------------
    public static final Edge e13 = new Edge("Не проверять");
    public static final Edge e12 = new Edge("Проверять");
    public static final Edge e23p = new Edge("Вероятность успеха");
    public static final Edge e23n = new Edge("Вероятность аварии");
    public static final Edge e34 = new Edge("Строить обычный реактор (без проверки)");
    public static final Edge e34p = new Edge("Строить обычный реактор (проверка-успех)");
    public static final Edge e35p = new Edge("Строить передовой реактор (проверка-успех)");
    public static final Edge e34n = new Edge("Строить обычный реактор (проверка-авария)");
    public static final Edge e35n = new Edge("Строить передовой реактор (проверка-авария)");
    public static final Edge e46s = new Edge("Вероятность успеха обычного реактора (без проверки)");
    public static final Edge e46f = new Edge("Вероятность аварии обычного реактора (без проверки)");
    public static final Edge e46ps = new Edge("Вероятность успеха обычного реактор (проверка-успех)");
    public static final Edge e46pf = new Edge("Вероятность аварии обычного реактора (проверка-успех)");
    public static final Edge e56ps = new Edge("Вероятность успеха передового реактора (проверка-успех)");
    public static final Edge e56pm = new Edge("Вероятность небольшой аварии передового реактора (проверка-успех)");
    public static final Edge e56pl = new Edge("Вероятность крупной аварии передового реактора (проверка-успех)");
    public static final Edge e46ns = new Edge("Вероятность успеха обычного реактора (проверка-авария)");
    public static final Edge e46nf = new Edge("Вероятность аварии обычного реактора (проверка-авария)");
    public static final Edge e56ns = new Edge("Вероятность успеха передового реактора (проверка-авария)");
    public static final Edge e56nm = new Edge("Вероятность небольшой аварии передового реактора (проверка-авария)");
    public static final Edge e56nl = new Edge("Вероятность крупной аварии передового реактора (проверка-авария)");

    public static String node1Decision;
    public static String node3pDecision;
    public static String node3nDecision;

    public static Graph<Vertex, Edge> getGraph() {
        Graph<Vertex, Edge> graph = new SparseMultigraph<>();
        graph.addEdge(e13, v1, v13, EdgeType.DIRECTED);
        graph.addEdge(e12, v1, v2, EdgeType.DIRECTED);
        graph.addEdge(e23p, v2, v23p, EdgeType.DIRECTED);
        graph.addEdge(e23n, v2, v23n, EdgeType.DIRECTED);
        graph.addEdge(e34, v13, v34, EdgeType.DIRECTED);
        graph.addEdge(e34p, v23p, v34p, EdgeType.DIRECTED);
        graph.addEdge(e35p, v23p, v35p, EdgeType.DIRECTED);
        graph.addEdge(e34n, v23n, v34n, EdgeType.DIRECTED);
        graph.addEdge(e35n, v23n, v35n, EdgeType.DIRECTED);
        graph.addEdge(e46s, v34, v46s, EdgeType.DIRECTED);
        graph.addEdge(e46f, v34, v46f, EdgeType.DIRECTED);
        graph.addEdge(e46ps, v34p, v46ps, EdgeType.DIRECTED);
        graph.addEdge(e46pf, v34p, v46pf, EdgeType.DIRECTED);
        graph.addEdge(e56ps, v35p, v56ps, EdgeType.DIRECTED);
        graph.addEdge(e56pm, v35p, v56pm, EdgeType.DIRECTED);
        graph.addEdge(e56pl, v35p, v56pl, EdgeType.DIRECTED);
        graph.addEdge(e46ns, v34n, v46ns, EdgeType.DIRECTED);
        graph.addEdge(e46nf, v34n, v46nf, EdgeType.DIRECTED);
        graph.addEdge(e56ns, v35n, v56ns, EdgeType.DIRECTED);
        graph.addEdge(e56nm, v35n, v56nm, EdgeType.DIRECTED);
        graph.addEdge(e56nl, v35n, v56nl, EdgeType.DIRECTED);
        return graph;
    }

    public static void solve() {
        v35n.value = e56ns.chance * e56ns.cost + e56nm.chance * e56nm.cost + e56nl.chance * e56nl.cost;
        v34n.value = e46ns.chance * e46ns.cost + e46nf.chance * e46nf.cost;
        v35p.value = e56ps.chance * e56ps.cost + e56pm.chance * e56pm.cost + e56pl.chance * e56pl.cost;
        v34p.value = e46ps.chance * e46ps.cost + e46pf.chance * e46pf.cost;
        v34.value = e46s.chance * e46s.cost + e46f.chance * e46f.cost;

        float v34nCOST = v34n.value + e34n.cost;
        float v35nCOST = v35n.value + e35n.cost;
        node3nDecision = defineOptimalDecision3(v34nCOST, v35nCOST);
        v23n.value = Math.max(v34nCOST, v35nCOST);

        float v34pCOST = v34p.value + e34p.cost;
        float v35pCOST = v35p.value + e35p.cost;
        node3pDecision = defineOptimalDecision3(v34pCOST, v35pCOST);
        v23p.value = Math.max(v34pCOST, v35pCOST);

        v13.value = v34.value + e34.cost;
        v2.value = v23p.value * e23p.chance + v23n.value * e23n.chance;
        float v13COST = v13.value + e13.cost;
        float v12COST = v2.value + e12.cost;
        node1Decision = defineOptimalDecision1(v13COST, v12COST);
        v1.value = Math.max(v13COST, v12COST);
    }

    private static String defineOptimalDecision1(float normal, float advanced) {
        if (normal < advanced) return "ПРОВЕРКА";
        if (normal > advanced) return "БЕЗ ПРОВЕРКИ";
        return "ВЫБОР НЕ ИМЕЕТ ЗНАЧЕНИЯ";
    }

    private static String defineOptimalDecision3(float normal, float advanced) {
        if (normal > advanced) return "ОБЫЧНЫЙ";
        if (normal < advanced) return "ПЕРЕДОВОЙ";
        return "ВЫБОР НЕ ИМЕЕТ ЗНАЧЕНИЯ";
    }

    public static class Vertex {
        public Vertex(int step, boolean ourChoice, String description) {
            this.step = step;
            this.ourChoice = ourChoice;
            this.description = description;
        }

        public int step;
        public float value;
        public boolean ourChoice;
        public String description;
    }

    public static class Edge {
        public Edge(String description) {
            this.description = description;
        }

        public String description;
        public int cost;
        public float chance;
    }
}
