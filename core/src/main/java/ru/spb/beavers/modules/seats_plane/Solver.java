package ru.spb.beavers.modules.seats_plane;

import com.sun.javafx.beans.annotations.NonNull;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Никита on 12.04.15.
 */
public class Solver implements attachCapable {

    private JPanel pane = new JPanel();

    private short SprosType = 1;
    private int PlaceNum = 0; /** количество мест M **/
    private int SprosValue = 0; /** величина спроса  **/
    private int PriceVf = 0; /** цена 1 **/
    private int PriceVd = 0; /** цена 2 **/


    public Solver()
    {
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
        pane.setBackground(Color.white);
    }

    class ResolveAlgorithm
    {
        double d0 = 0;
        double dStar = 0;
        double usefulValue = 0;

        public ResolveAlgorithm(){}


        public double resolveD0()
        {
            d0 = (2*PlaceNum + Math.sqrt(4*(Math.pow(PlaceNum,2)) - 8*(1- PriceVd/PriceVf)))/2;
            return d0;
        }

        public double decision(short type)
        {
           if(type==2)
           {
                if(d0>SprosValue)
                {
                    JLabel textLabl2 = new JLabel("<html><h2>d<sup>0</sup> &gt x => решение "+SprosValue+" мест под 2 класс; "+(PlaceNum - SprosValue)+" - под первый</h2></html>");
                    textLabl2.setAlignmentX(JComponent.CENTER_ALIGNMENT);
                    pane.add(textLabl2);
                    dStar=SprosValue;
                    return SprosValue;
                }
                else
                {
                    JLabel textLabl2 = new JLabel("<html><h2>d<sup>0</sup> &lt= x </br>Решение: "+d0+" мест под 2 класс; "+(PlaceNum - d0)+" - под первый </h2></html>");
                    textLabl2.setAlignmentX(JComponent.CENTER_ALIGNMENT);
                    pane.add(textLabl2);
                    dStar=d0;
                    return  d0;
                }
           }
           if(type==1)
           {
               if(SprosValue > PlaceNum)
               {
                   JLabel textLabl2 = new JLabel("<html><h2>y &gt= M </br>Решение: "+0+" мест под 2 класс; "+(PlaceNum - 0)+" - под первый </h2></html>");
                   textLabl2.setAlignmentX(JComponent.CENTER_ALIGNMENT);
                   pane.add(textLabl2);
                   dStar=0;
                   return 0;
               }
               else
               {
                   JLabel textLabl2 = new JLabel("<html><h2>y &lt M </br>Решение: "+(PlaceNum-SprosValue)+" мест под 2 класс; "+(PlaceNum - (PlaceNum-SprosValue))+" - под первый </h2></html>");
                   textLabl2.setAlignmentX(JComponent.CENTER_ALIGNMENT);
                   pane.add(textLabl2);
                   dStar=PlaceNum-SprosValue;
                   return PlaceNum-SprosValue;
               }
           }
           return  0;
        }

        public double usefulSize(short type)
        {
            if(type==2)
            {
                usefulValue = PriceVd * dStar + PriceVf*((PlaceNum-dStar) - 1/2*Math.pow((PlaceNum-dStar),3)/3);
                return  usefulValue;
            }
            if(type==1)
            {
                usefulValue = PriceVd *( dStar - 1/2* Math.pow(dStar,3)/3) + PriceVf*(PlaceNum-dStar);
                return  usefulValue;
            }
            return 0;
        }

    }
    public void initTerms(@NonNull short sprosT,@NonNull int sprosV, @NonNull int placeN, @NonNull int Vf, @NonNull int Vd)
    {
        SprosType = sprosT;
        PlaceNum = placeN;
        SprosValue = sprosV;
        PriceVf = Vf;
        PriceVd = Vd;
    }

    public void resolve()
    {
        pane.removeAll();
        JLabel Title = new JLabel("<html><h2>Пример решения</h2></html>");
        pane.add(Title);
        if(SprosType==2)
        {
            creatComponentAsist.setPicture(pane,TaskCommon.PIC_RESOLVE_1);
            String formulaSymb = "do=\\frac{2M + \\sqrt {4M^2-8(1- \\frac{Vd}{Vf})}}{2}";
            String formulaNum = "\\frac{2*"+PlaceNum + " \\sqrt {4*"+PlaceNum + "^2-8*(1- \\frac{"+PriceVd+"}{"+PriceVf+"})}}{2}";

            JLabel textLabl1 = new JLabel("<html><h2>Расчет d0:</h2></html>");
            textLabl1.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
            pane.add(textLabl1);

            ResolveAlgorithm resAlg = new ResolveAlgorithm();
            JLabel formLabel = creatComponentAsist.formula(formulaSymb+"="+formulaNum+"="+(int)resAlg.resolveD0());
            formLabel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            pane.add(formLabel);


            JLabel textLabl2 = new JLabel("<html><h2>Расчет d**:</h2></html>");
            textLabl2.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
            pane.add(textLabl2);

            double result = resAlg.decision(SprosType);

            JLabel textLabl3 = new JLabel("<html><h2>Расчет полезности d**:</h2></html>");
            textLabl3.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
            pane.add(textLabl3);
            creatComponentAsist.setPicture(pane,TaskCommon.PIC_RESOLVE_USE_FORMULA);
            JLabel textLabl4 = new JLabel("<html>Подставим значения:</html>");
            textLabl4.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
            pane.add(textLabl4);

            String formulaSymb2 = "U(d^{**})=Vd*d^{**} + Vf[ (M-d^{**}) - \\frac{1}{2} * \\frac{(M-d^{**})^3}{3} ] ";
            String formulaNum2 = ""+PriceVd+"*"+result+"+" + PriceVf+"[ ("+PlaceNum+"-"+result+") - \\frac{1}{2} * \\frac{("+PlaceNum+"-"+result+")^3}{3} ]";
            JLabel formLabel2 = creatComponentAsist.formula(formulaSymb2+"="+formulaNum2+"="+(int)resAlg.usefulSize(SprosType));
            formLabel2.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            pane.add(formLabel2);

        }
        else
        {
            creatComponentAsist.setPicture(pane,TaskCommon.PIC_RESOLVE_2);
            JLabel textLabl2 = new JLabel("<html><h2>Расчет d**:</h2></html>");
            textLabl2.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
            pane.add(textLabl2);

            ResolveAlgorithm resAlg = new ResolveAlgorithm();
            double result = resAlg.decision(SprosType);

            JLabel textLabl3 = new JLabel("<html><h2>Расчет полезности d**:</h2></html>");
            textLabl3.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
            pane.add(textLabl3);
            creatComponentAsist.setPicture(pane,TaskCommon.PIC_RESOLVE_USE_FORMULA2);
            JLabel textLabl4 = new JLabel("<html>Подставим значения:</html>");
            textLabl4.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
            pane.add(textLabl4);

            String formulaSymb2 = "U(d^{**})=Vd * (d^{**} - \\frac{1}{2} * \\frac{d^{**^3}}{3}) + Vf(M-d^{**}) ";
            String formulaNum2 = ""+PriceVd+"*("+result+"-" + "\\frac{1}{2} * \\frac{"+result+"^3}{3})+Vf("+PlaceNum+"-"+result+")";
            JLabel formLabel2 = creatComponentAsist.formula(formulaSymb2+"="+formulaNum2+"="+(int)resAlg.usefulSize(SprosType));
            formLabel2.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            pane.add(formLabel2);
        }


    }

    @Override
    public void attachToExPanel(@NonNull JPanel exPanel)
    {
        exPanel.add(pane);
    }
}
