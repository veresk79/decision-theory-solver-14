package ru.spb.beavers.modules.optimization_of_binary_relation_br18;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Belyaev Andrey on 11.04.2015.
 */
public class OptimizationOfBinaryRelations {
    public boolean checkForStrictOrder(BinaryRelation relation) {
        BinaryElement lastElement = null;

        // Transitive:
        for (BinaryElement elem : relation.getElements()) {
            if (!checkElementForTransitive(elem)) {
                System.out.println("Check Element For Transitive: false");
                return false;
            }
            if (!checkElementForAntireflexive(elem)) {
                System.out.println("Check Element For Antyreflecsive: false");
                return false;
            }
            if (!checkElementForAsymmetric(elem)) {
                System.out.println("Check Element For Unsymmetric: false");
                return false;
            }

            if (elem.getRelationTo().size() == 0) {
                if (lastElement == null) {
                    lastElement = elem;
                } else {
                    System.out.println("Found second max element. false");
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkElementForTransitive(BinaryElement element) {
        for (BinaryElement firstConnectedElement : element.getRelationTo()) {
            for (BinaryElement secondConnectedElement : firstConnectedElement.getRelationTo()) {
                if (!element.equals(secondConnectedElement) && !element.getRelationTo().contains(secondConnectedElement)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkElementForAntireflexive(BinaryElement element) {
        for (BinaryElement connectedElement : element.getRelationTo()) {
            if (element.equals(connectedElement)) {
                return false;
            }
        }
        return true;
    }

    private boolean checkElementForAsymmetric(BinaryElement element) {
        for (BinaryElement connectedElement : element.getRelationTo()) {
            if (connectedElement.getRelationTo().contains(element)) {
                return false;
            }
        }
        return true;
    }

    public BinaryRelation additionOperation(BinaryRelation relation) {
        BinaryRelation additionResult = new BinaryRelation();
        for (BinaryElement element1 : relation.getElements()) {
            for (BinaryElement element2 : relation.getElements()) {
                if (!element1.getRelationTo().contains(element2)) {
                    additionResult.addConnection(element1.getName(), element2.getName());
                }
            }
        }
        return additionResult;
    }

    public BinaryRelation revertOperation(BinaryRelation relation) {
        BinaryRelation revertResult = new BinaryRelation();
        for (BinaryElement element : relation.getElements()) {
            for (BinaryElement connectedElement : element.getRelationTo()) {
                revertResult.addConnection(connectedElement.getName(), element.getName());
            }
        }
        return revertResult;
    }

    public BinaryRelation getDualRelation(BinaryRelation relation) {
        BinaryRelation resultRelation;
        resultRelation = additionOperation(relation);
        resultRelation = revertOperation(resultRelation);
        return resultRelation;
    }

    public BinaryRelation getEquivalenceRelation(BinaryRelation relation) {
        BinaryRelation resultRelation = getDualRelation(relation);

        for (BinaryElement elementFromInitRelation : relation.getElements()) {
            for (BinaryElement connectedElement : elementFromInitRelation.getRelationTo()) {
                resultRelation.removeConnection(elementFromInitRelation, connectedElement);
            }
        }

        return resultRelation;
    }

    public BinaryRelation getNonstrictRelationFromStrict(BinaryRelation strictRelation) {
        BinaryRelation resultRelation = getDualRelation(strictRelation);

        for (BinaryElement element : getDualRelation(strictRelation).getElements()) {
            for (BinaryElement connectedElement : element.getRelationTo()) {
                if (!element.equals(connectedElement)) {
                    if (!strictRelation.elementIsReachableFromOther(element, connectedElement)) {
                        resultRelation.removeConnection(element, connectedElement);
                    }
                }
            }
        }

        return resultRelation;
    }

    public List<BinaryElement> getOptimumElement(BinaryRelation strictRelation, BinaryRelation nonStrictRelation,int method) {
        List<BinaryElement> result = new ArrayList<BinaryElement>();

        switch (method) {
            case 1:
                for (BinaryElement elementX : strictRelation.getElements()) {
                    boolean xElementIsResult = true;
                    for (BinaryElement elementY : strictRelation.getElements()) {
                        if (elementY.getRelationTo().contains(elementX)) {
                            xElementIsResult = false;
                            break;
                        }
                    }
                    if (xElementIsResult) {
                        result.add(elementX);
                    }
                }
                break;
            case 2:
                for (BinaryElement elementX : strictRelation.getElements()) {
                    boolean xElementIsResult = true;
                    for (BinaryElement elementY : strictRelation.getElements()) {
                        if (!elementX.getRelationTo().contains(elementY) && !elementX.equals(elementY)) {
                            xElementIsResult = false;
                            break;
                        }
                    }
                    if (xElementIsResult) {
                        result.add(elementX);
                    }
                }
                break;
            case 3:
                for (BinaryElement elementX : nonStrictRelation.getElements()) {
                    boolean xElementIsResult = true;
                    for (BinaryElement elementY : strictRelation.getElements()) {
                        if (elementY.getRelationTo().contains(elementX)) {
                            if (!elementX.getRelationTo().contains(elementY)) {
                                xElementIsResult = false;
                                break;
                            }
                        }
                    }
                    if (xElementIsResult) {
                        result.add(elementX);
                    }
                }
                break;
            case 4:
                for (BinaryElement elementX : nonStrictRelation.getElements()) {
                    boolean xElementIsResult = true;
                    for (BinaryElement elementY : strictRelation.getElements()) {
                        if (elementY.getRelationTo().contains(elementX)) {
                            if (!elementX.equals(elementY)) {
                                xElementIsResult = false;
                                break;
                            }
                        }
                    }
                    if (xElementIsResult) {
                        result.add(elementX);
                    }
                }
                break;
            case 5:
                for (BinaryElement elementX : nonStrictRelation.getElements()) {
                    boolean xElementIsResult = true;
                    for (BinaryElement elementY : nonStrictRelation.getElements()) {
                        if (!elementX.getRelationTo().contains(elementY)) {
                            xElementIsResult = false;
                            break;
                        }
                    }
                    if (xElementIsResult) {
                        result.add(elementX);
                    }
                }
                break;
            case 6:
                for (BinaryElement elementX : nonStrictRelation.getElements()) {
                    boolean xElementIsResult = true;
                    for (BinaryElement elementY : nonStrictRelation.getElements()) {
                        if (!elementX.getRelationTo().contains(elementY)) {
                            xElementIsResult = false;
                            break;
                        }
                        if (elementY.getRelationTo().contains(elementX) && !elementX.equals(elementY)) {
                            xElementIsResult = false;
                            break;
                        }
                    }
                    if (xElementIsResult) {
                        result.add(elementX);
                    }
                }
                break;
            default:
                throw new IllegalArgumentException("Incorrect method number");
        }
        return result;
        //throw new IllegalStateException("Error, optimum element not found");
    }
}
