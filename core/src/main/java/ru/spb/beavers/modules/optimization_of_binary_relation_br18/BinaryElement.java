package ru.spb.beavers.modules.optimization_of_binary_relation_br18;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Belyaev Andrey on 11.04.2015.
 */
public class BinaryElement {
    private String name;
    private List<BinaryElement> relationTo = new ArrayList<BinaryElement>();

    public BinaryElement(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean addRelationTo(BinaryElement element) {
        if (relationTo.contains(element)) {
            return false;
        } else {
            return relationTo.add(element);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BinaryElement element = (BinaryElement) o;

        if (name != null ? !name.equals(element.name) : element.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        return result;
    }

    @Override
    public String toString() {
        return name;
    }

    public List<BinaryElement> getRelationTo() {
        return relationTo;
    }
}
