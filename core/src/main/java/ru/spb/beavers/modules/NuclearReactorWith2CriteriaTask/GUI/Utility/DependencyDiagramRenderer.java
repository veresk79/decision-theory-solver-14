package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.RenderContext;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import edu.uci.ics.jung.visualization.transform.shape.GraphicsDecorator;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

/**
 * Created by Alexandr on 01.04.2015.
 */
public class DependencyDiagramRenderer implements Renderer.Vertex<VertexShape, String> {
    @Override public void paintVertex(RenderContext<VertexShape, String> rc,
                                      Layout<VertexShape, String> layout, VertexShape vertex) {
        GraphicsDecorator graphicsContext = rc.getGraphicsContext();
        Point2D center = layout.transform(vertex);
        java.awt.Shape shape = null;
        Color color2 = null;
        Color color1 = new Color(0,0,0);
        if(vertex.shape.equals("Square"))
        {
            shape = new Rectangle((int)center.getX()-10, (int)center.getY()-10, 20, 20);
            color2 = new Color(0, 225, 225);
        }
        else if(vertex.shape.equals("Rhomb"))
        {
            Polygon p = new Polygon();
            p.addPoint((int)center.getX(), (int)center.getY()-12);
            p.addPoint((int)center.getX()-12, (int)center.getY());
            p.addPoint((int)center.getX(), (int)center.getY()+12);
            p.addPoint((int)center.getX()+12, (int)center.getY());
            shape = p;
            color2 = new Color(0, 225, 0);
        }
        else if(vertex.shape.equals("Circle"))
        {
            shape = new Ellipse2D.Double(center.getX()-10, center.getY()-10, 20, 20);
            color2 = new Color(0, 127, 127);
        }
        else if(vertex.shape.equals("Dot"))
        {
            shape = new Ellipse2D.Double(center.getX()-3, center.getY()-3, 6, 6);
            color2 = new Color(0, 0, 0);
        }
        graphicsContext.setPaint(color2);
        graphicsContext.fill(shape);
        graphicsContext.setPaint(color1);
        graphicsContext.draw(shape);
    }
}