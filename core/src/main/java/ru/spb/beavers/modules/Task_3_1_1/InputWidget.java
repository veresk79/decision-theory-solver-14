package ru.spb.beavers.modules.Task_3_1_1;

import com.sun.javafx.beans.annotations.NonNull;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Никита on 12.04.15.
 */
public class InputWidget implements attachCapable {

    private JPanel  MAIN_PANEL = new JPanel();;
    private JTextField PlaceNumField=new JTextField(12);
    private JTextField SprosValueField=new JTextField(12);
    private JTextField PriceVfField=new JTextField(12);
    private JTextField PriceVdField=new JTextField(12);
    private JRadioButton buttonClass1= new JRadioButton(TaskCommon.SPROS_CLASS_1);
    private JRadioButton buttonClass2= new JRadioButton(TaskCommon.SPROS_CLASS_2);

    private short SprosType = 1;
    private int PlaceNum = 0; /** количество мест M **/
    private int SprosValue = 0; /** величина спроса  **/
    private int PriceVf = 0; /** цена 1 **/
    private int PriceVd = 0; /** цена 2 **/

    public InputWidget()
    {
        MAIN_PANEL.setLayout(new BoxLayout(MAIN_PANEL, BoxLayout.Y_AXIS));
        defaultParam();
        initTextFields();
        buttonClass1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SprosType = 1;

            }
        });
        buttonClass2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SprosType = 2;

            }
        });
        ButtonGroup group = new ButtonGroup();
        group.add(buttonClass1);
        group.add(buttonClass2);
        JLabel Title = new JLabel("<html><h2>Данные для ввода</h2></html>");
        MAIN_PANEL.add(new JPanel().add(Title));
        MAIN_PANEL.add(creatComponentAsist.customPanel(TaskCommon.SPROS_CLASS_TYPE, buttonClass1, buttonClass2));
        MAIN_PANEL.add(creatComponentAsist.customPanel(TaskCommon.SPROS_SIZE, SprosValueField));
        MAIN_PANEL.add(creatComponentAsist.customPanel(TaskCommon.COUNT_PLACE, PlaceNumField));
        MAIN_PANEL.add(creatComponentAsist.customPanel(TaskCommon.PRICE_CLASS_1, PriceVfField));
        MAIN_PANEL.add(creatComponentAsist.customPanel(TaskCommon.PRICE_CLASS_2, PriceVdField));
    }

    @Override
    public void attachToExPanel(@NonNull JPanel exPanel)
    {
        exPanel.add(MAIN_PANEL);
    }

    private void initTextFields()
    {
        SprosValueField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("Text SV=" + SprosValueField.getText());
                System.out.println("Spros type=" + SprosType);

                try{
                    SprosValue = Integer.parseInt(SprosValueField.getText());
                }
                catch(NumberFormatException ex)
                {
                    SprosValue = 70;
                    JOptionPane.showMessageDialog(null, "Ошибка при вводе спроса!");
                    ex.printStackTrace();
                }
            }
        });

        PlaceNumField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("Text PN=" + PlaceNumField.getText());
                try{
                    PlaceNum = Integer.parseInt(PlaceNumField.getText());
                }
                catch(NumberFormatException ex)
                {
                    PlaceNum = 100;
                    JOptionPane.showMessageDialog(null, "Ошибка при вводе количества мест!");
                    ex.printStackTrace();
                }
                if(PlaceNum < SprosValue)
                    JOptionPane.showMessageDialog(null, "Количество мест должно быть >= спросу");
            }
        });

        PriceVfField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("Text Vf=" + PriceVfField.getText());
                try{
                    PriceVf = Integer.parseInt(PriceVfField.getText());
                }
                catch(NumberFormatException ex)
                {
                    PriceVf = 1400;
                    JOptionPane.showMessageDialog(null, "Ошибка при вводе Vf!");
                    ex.printStackTrace();
                }
            }
        });

        PriceVdField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("Text Vd=" + PriceVdField.getText());
                try{
                    PriceVd = Integer.parseInt(PriceVdField.getText());
                }
                catch(NumberFormatException ex)
                {
                    PriceVd = 700;
                    JOptionPane.showMessageDialog(null, "Ошибка при вводе Vd!");
                    ex.printStackTrace();
                }
                if(PriceVf <= PriceVd)
                    JOptionPane.showMessageDialog(null, "Стоимость билета II класса должна быть меньше стоимость билета I класса");
            }
        });


    }

    public void setDefaultFieldValues()
    {
        buttonClass2.setSelected(true);
        SprosValueField.setText("70");
        PlaceNumField.setText("100");
        PriceVfField.setText("1400");
        PriceVdField.setText("700");

    }


    public String toString()
    {
        String asString = ""+SprosType+"\n"+
                           PlaceNum+"\n"+
                           SprosValue +"\n"+
                           PriceVf + "\n" +
                           PriceVd ;
        return asString;
    }

    public void loadData(String dataString)
    {
        Pattern pattern = Pattern.compile("((\\\"[^\"]+\\\")|\\S+)");
        Matcher matcher = pattern.matcher(dataString);
        for(int i=0; i<5 && matcher.find(); i++)
        {
            try{
                switch (i)
                {
                    case 0:
                    {

                            if(Integer.parseInt(matcher.group())==1)
                            {
                                buttonClass1.setSelected(true);
                                SprosType=1;
                            }
                            else
                            {
                                buttonClass2.setSelected(true);
                                SprosType=2;
                            }

                    }
                    break;
                    case 1:
                    {
                        SprosValueField.setText(matcher.group());

                    }
                    break;
                    case 2:
                    {
                        PlaceNumField.setText(matcher.group());

                    }
                    break;
                    case 3:
                    {
                        PriceVfField.setText(matcher.group());

                    }
                    break;
                    case 4:
                    {
                        PriceVdField.setText(matcher.group());

                    }
                    break;

                }
            }
            catch (Exception e)
            {
                System.out.println("LOG:: load file exception");
                e.printStackTrace();
            }
        }

}


    private void defaultParam()
    {
        SprosType = 2; // тип спроса
        PlaceNum = 100; /** количество мест M **/
        SprosValue = 70; /** величина спроса  **/
        PriceVf = 1400; /** цена 1 **/
        PriceVd = 700; /** цена 2 **/
    }

    public short getSprosType()
    {
        return SprosType;
    }

    public int getPlaceNum()
    {
        try{
            PlaceNum = Integer.parseInt(PlaceNumField.getText());
            if(checkMinus(PlaceNum)) throw new Exception("Ниже нуля");
        }
        catch (Exception ex)
        {
            printAlert("Количество мест");
            ex.printStackTrace();
            PlaceNum = 100;
        }
        return PlaceNum;
    }

    public int getSprosValue()
    {
        try{
            SprosValue = Integer.parseInt(SprosValueField.getText());
            if(checkMinus(SprosValue)) throw new Exception("Ниже нуля");
        }
        catch (Exception ex)
        {
            printAlert("Спрос");
            ex.printStackTrace();
            SprosValue = 70;
        }
        return SprosValue;
    }

    public int getPriceVf()
    {
        try{
            PriceVf = Integer.parseInt(PriceVfField.getText());
            if(checkMinus(PriceVf)) throw new Exception("Ниже нуля");
        }
        catch (Exception ex)
        {
            printAlert("Цена 1 класса");
            ex.printStackTrace();
            PriceVf = 1400;
        }
        return PriceVf;
    }
    public int getPriceVd()
    {
        try{
            PriceVd = Integer.parseInt(PriceVdField.getText());
            if(checkMinus(PriceVd)) throw new Exception("Ниже нуля");
        }
        catch (Exception ex)
        {
            printAlert("Цена 2 класса");
            ex.printStackTrace();
            PriceVd = 700;
        }
        return PriceVd;
    }

    public void printAlert(String fieldName)
    {
        JOptionPane.showMessageDialog(null, "Ошибка при вводе значения; поле "+fieldName+". Значение установлено по умолчанию");
    }

    public boolean checkMinus(double value)
    {
        if(value<0) return true;
        else return  false;
    }

}
