package ru.spb.beavers.modules.hotelrooms.GUI;

import java.awt.Dimension;

import javax.swing.JPanel;

import ru.spb.beavers.modules.hotelrooms.GUI.Utility.GUIElement;
import ru.spb.beavers.modules.hotelrooms.GUI.Utility.LastElCoords;

public class SolutionPanel {

    private LastElCoords last_el_coords;

    public SolutionPanel(JPanel panel)
    {
        last_el_coords = new LastElCoords();

        panel.setLayout(null);
        panel.setSize(760, 4700);
        panel.setPreferredSize(new Dimension(750, 4700));

        // Вычисления в случае двух атрибутов
        GUIElement.setTextHeader("Выбор решения (с точки зрения владельца гостиницы).", 20, 300, 30, last_el_coords, panel);
        last_el_coords.updateValues(10, 400);
        GUIElement.setHtmlText600Px("./../html/solution_1.html", last_el_coords, panel);

        // Вычисления в случае линейных компромиссов
        GUIElement.setTextHeader("Выбор решения (с точки зрения консолидатора).", 30, 400, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 400);
        GUIElement.setHtmlText600Px("./../html/solution_2.html", last_el_coords, panel);

        // Расчет полезностей решений.
        GUIElement.setTextHeader("Выбор решения (взаимовыгодного с точки зрения владельца гостиницы и консолидатора).", 30, 400, 30, last_el_coords, panel);
        last_el_coords.updateValues(10, 550);
        GUIElement.setHtmlText600Px("./../html/solution_3.html", last_el_coords, panel);
    }
}