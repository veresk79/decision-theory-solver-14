package ru.spb.beavers.modules;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import ru.spb.beavers.modules.optimization_of_binary_relation_br18.BinaryElement;
import ru.spb.beavers.modules.optimization_of_binary_relation_br18.BinaryRelation;
import ru.spb.beavers.modules.optimization_of_binary_relation_br18.OptimizationOfBinaryRelations;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class OptimizationOfBinaryRelationBr18Module implements ITaskModule {
    private OptimizationOfBinaryRelations optimizationOfBinaryRelationsService;
    private BinaryRelation initRelation;
    private VisualizationViewer graphVizualizationViewer;

    boolean descriptionInitialized;
    boolean solutionInitialized;
    boolean inputInitialized;
    boolean exampleInitialized;

    private JPanel graphPanel;
    private JPanel descriptionPanel;
    private JLabel taskDescription;
    private JPanel solutionPanel;
    private JPanel inputPanel;
    private JPanel examplePanel;

    private JTextField vertex;
    private JTextField edgeFromVertex;
    private JTextField edgeToVertex;

    public ActionListener defaultValuesListener;
    public ActionListener saveValuesListener;
    public ActionListener loadValuesListener;

    public OptimizationOfBinaryRelationBr18Module() {
        optimizationOfBinaryRelationsService = new OptimizationOfBinaryRelations();
        initRelation = new BinaryRelation();

        defaultValuesListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                initRelation = new BinaryRelation();
                initRelation.addConnection("a", "b");
                initRelation.addConnection("b", "c");
                initRelation.addConnection("c", "d");
                initRelation.addConnection("a", "c");
                initRelation.addConnection("a", "d");
                initRelation.addConnection("b", "d");
                refreshGraph();
            }
        };
        saveValuesListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser saveDialog = new JFileChooser();
                saveDialog.showSaveDialog(inputPanel);
                File saveFile = saveDialog.getSelectedFile();
                try {
                    FileWriter writer = new FileWriter(saveFile);
                    writer.write("Elements:\r\n");
                    for (BinaryElement element : initRelation.getElements()) {
                        writer.write(element.getName() + "\r\n");
                    }
                    writer.write("Edges:\r\n");
                    for (BinaryElement element : initRelation.getElements()) {
                        for (BinaryElement connectedTo : element.getRelationTo()) {
                            writer.write(element.getName() + "\r\n");
                            writer.write(connectedTo.getName() + "\r\n");
                        }
                    }
                    writer.write("End\r\n");
                    writer.close();
                }  catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        loadValuesListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser openDialog = new JFileChooser();
                openDialog.showOpenDialog(inputPanel);
                File openFile = openDialog.getSelectedFile();
                try {
                    Scanner scanner = new Scanner(openFile);
                    initRelation = new BinaryRelation();

                    String nextLine;
                    nextLine = scanner.nextLine();
                    if (!nextLine.equals("Elements:")) {
                        throw new IllegalArgumentException("Incorrect format of file");
                    }

                    while (true) {
                        nextLine = scanner.nextLine();
                        if (nextLine.equals("Edges:")) break;
                        initRelation.addOrGetElement(nextLine);
                    }

                    String secondLine;
                    while (true) {
                        nextLine = scanner.nextLine();
                        if (nextLine.equals("End")) break;
                        secondLine = scanner.nextLine();
                        if (nextLine.equals("End")) break;
                        initRelation.addConnection(nextLine, secondLine);
                    }

                    refreshGraph();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (NoSuchElementException e) {
                    throw new IllegalArgumentException("Incorrect format of file");
                }
            }
        };

        descriptionInitialized = false;
        solutionInitialized = false;
        inputInitialized = false;
    }

    public void initDescriptionObjects() {
        descriptionPanel = new JPanel();
        Dimension dim = new Dimension(750, 700);
        descriptionPanel.setMaximumSize(dim);
        descriptionPanel.setSize(dim);
        descriptionPanel.setMinimumSize(dim);
        descriptionPanel.setPreferredSize(dim);
        descriptionPanel.setBackground(Color.white);
        descriptionPanel.setBorder(BorderFactory.createEtchedBorder());

        URL resource = getClass().getResource("optimization_of_binary_relation_br18/br18_description.png");
        File f = new File(resource.getFile());

        try {
            BufferedImage myPicture = ImageIO.read(f);
            JLabel picLabel = new JLabel(new ImageIcon(myPicture));
            descriptionPanel.add(picLabel);
        } catch (IOException e) {
            e.printStackTrace();
        }

        descriptionInitialized = true;
    }

    public void initSolutionObjects() {
        solutionPanel = new JPanel();

        solutionPanel.setLayout(new FlowLayout());
        Dimension dim = new Dimension(750, 1250);
        solutionPanel.setMaximumSize(dim);
        solutionPanel.setSize(dim);
        solutionPanel.setMinimumSize(dim);
        solutionPanel.setPreferredSize(dim);
        solutionPanel.setBorder(BorderFactory.createEtchedBorder());
        solutionPanel.setBackground(Color.white);

        URL resource = getClass().getResource("optimization_of_binary_relation_br18/br18_theory.png");
        File f = new File(resource.getFile());

        try {
            BufferedImage myPicture = ImageIO.read(f);
            JLabel picLabel = new JLabel(new ImageIcon(myPicture));
            solutionPanel.add(picLabel);
        } catch (IOException e) {
            e.printStackTrace();
        }

        solutionInitialized = true;
    }

    public void initInputObjects() {
        graphPanel = new JPanel();
        Graph<String,String> graph = initRelation.getGraph();

        FRLayout layout = new FRLayout<>(graph);
        layout.setSize(new Dimension(300,300));
        graphVizualizationViewer = new VisualizationViewer<>(layout);
        graphVizualizationViewer.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        graphVizualizationViewer.getRenderer().getVertexLabelRenderer().setPosition(edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position.CNTR);
        graphVizualizationViewer.setSize(new Dimension(300, 300));

        graphPanel.setLayout(new BoxLayout(graphPanel, BoxLayout.PAGE_AXIS));
        Dimension graphDimension = new Dimension(350, 350);
        graphPanel.setMaximumSize(graphDimension);
        graphPanel.setSize(graphDimension);
        graphPanel.setMinimumSize(graphDimension);
        graphPanel.setPreferredSize(graphDimension);
        graphPanel.setBorder(BorderFactory.createEtchedBorder());
        graphPanel.add(graphVizualizationViewer);

        JButton addVertexButton = new JButton("Добавить");
        JButton removeVertexButton = new JButton("Удалить");
        JButton addEdgeButton = new JButton("Добавить");
        JButton removeEdgeButton = new JButton("Удалить");

        vertex = new JFormattedTextField();
        vertex.setColumns(4);
        vertex.setMaximumSize(new Dimension(50, 25));
        edgeFromVertex = new JFormattedTextField();
        edgeFromVertex.setColumns(4);
        edgeFromVertex.setMaximumSize(new Dimension(50, 25));
        edgeToVertex = new JFormattedTextField();
        edgeToVertex.setColumns(4);
        edgeToVertex.setMaximumSize(new Dimension(50, 25));

        inputPanel = new JPanel();
        inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.LINE_AXIS));
        Dimension dim = new Dimension(750, 380);
        inputPanel.setBackground(Color.WHITE);
        inputPanel.setMaximumSize(dim);
        inputPanel.setSize(dim);
        inputPanel.setMinimumSize(dim);
        inputPanel.setPreferredSize(dim);
        inputPanel.setBorder(BorderFactory.createEtchedBorder());

        Container leftContainer = new Container();
        leftContainer.setLayout(new BoxLayout(leftContainer, BoxLayout.Y_AXIS));
        leftContainer.add(Box.createRigidArea(new Dimension(370, 0)));
        leftContainer.add(graphPanel);
        inputPanel.add(leftContainer);

        JPanel rightContainer = new JPanel();
        rightContainer.setMinimumSize(new Dimension(400, 450));
        rightContainer.setMaximumSize(new Dimension(400, 450));
        rightContainer.setBackground(Color.WHITE);
        rightContainer.setLayout(new BoxLayout(rightContainer, BoxLayout.Y_AXIS));
        rightContainer.add(Box.createRigidArea(new Dimension(390, 25)));

        Container vertexChangeContainer = new Container();
        JLabel vertexContainerLabel = new JLabel("Вершины:");
        Container vertexContainer = new Container();
        vertexContainer.setLayout(new BoxLayout(vertexContainer, BoxLayout.X_AXIS));
        vertexContainer.add(vertexContainerLabel);
        rightContainer.add(vertexContainer);
        vertexChangeContainer.setLayout(new BoxLayout(vertexChangeContainer, BoxLayout.X_AXIS));
        JLabel vertexLabel = new JLabel("Имя вершины:");
        vertexChangeContainer.add(vertexLabel);
        vertexChangeContainer.add(Box.createRigidArea(new Dimension(10, 10)));
        vertexChangeContainer.add(vertex);
        vertexChangeContainer.add(Box.createRigidArea(new Dimension(10, 25)));
        vertexChangeContainer.add(addVertexButton);
        vertexChangeContainer.add(Box.createRigidArea(new Dimension(10, 25)));
        vertexChangeContainer.add(removeVertexButton);

        rightContainer.add(Box.createRigidArea(new Dimension(400, 10)));
        rightContainer.add(vertexChangeContainer);
        rightContainer.add(Box.createRigidArea(new Dimension(400, 30)));

        JLabel edgeContainerLabel = new JLabel("Дуги:");
        Container edgeContainer = new Container();
        edgeContainer.setLayout(new BoxLayout(edgeContainer, BoxLayout.X_AXIS));
        edgeContainer.add(edgeContainerLabel);
        rightContainer.add(edgeContainer);

        Container firstRowEdgeChangeContainer = new Container();
        firstRowEdgeChangeContainer.setLayout(new BoxLayout(firstRowEdgeChangeContainer, BoxLayout.X_AXIS));
        JLabel edgeFromLabel = new JLabel("От вершины:");
        firstRowEdgeChangeContainer.add(edgeFromLabel);
        firstRowEdgeChangeContainer.add(Box.createRigidArea(new Dimension(10, 10)));
        firstRowEdgeChangeContainer.add(edgeFromVertex);
        firstRowEdgeChangeContainer.add(Box.createRigidArea(new Dimension(10, 10)));
        JLabel edgeToLabel = new JLabel("До вершины:");
        firstRowEdgeChangeContainer.add(edgeToLabel);
        firstRowEdgeChangeContainer.add(Box.createRigidArea(new Dimension(10, 10)));
        firstRowEdgeChangeContainer.add(edgeToVertex);

        Container secondRowEdgeChangeContainer = new Container();
        secondRowEdgeChangeContainer.setLayout(new BoxLayout(secondRowEdgeChangeContainer, BoxLayout.X_AXIS));
        secondRowEdgeChangeContainer.add(Box.createRigidArea(new Dimension(10, 25)));
        secondRowEdgeChangeContainer.add(addEdgeButton);
        secondRowEdgeChangeContainer.add(Box.createRigidArea(new Dimension(10, 25)));
        secondRowEdgeChangeContainer.add(removeEdgeButton);

        rightContainer.add(Box.createRigidArea(new Dimension(400, 10)));
        rightContainer.add(firstRowEdgeChangeContainer);
        rightContainer.add(Box.createRigidArea(new Dimension(400, 10)));
        rightContainer.add(secondRowEdgeChangeContainer);
        rightContainer.add(Box.createRigidArea(new Dimension(400, 10)));

        inputPanel.add(rightContainer);
        inputInitialized = true;

        addVertexButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (vertex.getText().isEmpty()) return;
                initRelation.addOrGetElement(vertex.getText());
                refreshGraph();
            }
        });
        removeVertexButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (vertex.getText().isEmpty()) return;
                initRelation.removeElement(vertex.getText());
                refreshGraph();
            }
        });
        addEdgeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (edgeFromVertex.getText().isEmpty() || edgeToVertex.getText().isEmpty()) return;
                initRelation.addConnection(edgeFromVertex.getText(), edgeToVertex.getText());
                refreshGraph();
            }
        });
        removeEdgeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (edgeFromVertex.getText().isEmpty() || edgeToVertex.getText().isEmpty()) return;
                initRelation.removeConnection(edgeFromVertex.getText(), edgeToVertex.getText());
                refreshGraph();
            }
        });
    }

    private void refreshGraph() {
        if (graphVizualizationViewer != null) {
            Graph<String, String> graph = initRelation.getGraph();
            FRLayout layout = new FRLayout<>(graph);
            layout.setSize(new Dimension(300, 300));
            graphVizualizationViewer.setGraphLayout(layout);
        }
    }

    @Override
    public String getTitle() {
        return "Оптимизация по бинарному\nотношению (1-3)";
    }

    @Override
    public void initDescriptionPanel(JPanel panel) {
        if (!descriptionInitialized) {
            initDescriptionObjects();
            panel.add(descriptionPanel);
        } else {
            panel.add(descriptionPanel);
        }
    }

    @Override
    public void initSolutionPanel(JPanel panel) {
        if (!solutionInitialized) {
            initSolutionObjects();
            panel.add(solutionPanel);
        } else {
            panel.add(solutionPanel);
        }
    }

    @Override
    public void initInputPanel(JPanel panel) {
        if (!inputInitialized) {
            initInputObjects();
            panel.add(inputPanel);
        } else {
            panel.add(inputPanel);
        }
    }

    @Override
    public void initExamplePanel(JPanel panel) {
        if (!exampleInitialized) {
            examplePanel = new JPanel();
        }

        examplePanel.removeAll();
        if (!optimizationOfBinaryRelationsService.checkForStrictOrder(initRelation)) {
            JOptionPane.showMessageDialog(null, "Введенно бинарное отношение, для которого не выполняется строгий порядок");
            return;
        }

        initExampleObjects();
        panel.add(examplePanel);
    }

    private void initExampleObjects() {
//        examplePanel = new JPanel();
        examplePanel.setLayout(new FlowLayout());
        Dimension dim = new Dimension(750, 2800);
        examplePanel.setMaximumSize(dim);
        examplePanel.setSize(dim);
        examplePanel.setMinimumSize(dim);
        examplePanel.setPreferredSize(dim);
        examplePanel.setBackground(Color.WHITE);
        examplePanel.setBorder(BorderFactory.createEtchedBorder());

        Container container = new Container();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setMaximumSize(new Dimension(50, 100));
        container.setMinimumSize(new Dimension(50, 100));

        BinaryRelation additionOperationResult = optimizationOfBinaryRelationsService.additionOperation(initRelation);
        BinaryRelation revertOperationResult = optimizationOfBinaryRelationsService.revertOperation(initRelation);
        BinaryRelation dualRelation = optimizationOfBinaryRelationsService.getDualRelation(initRelation);
        BinaryRelation equivalenceRelation = optimizationOfBinaryRelationsService.getEquivalenceRelation(initRelation);
        BinaryRelation nonstrictRelation = optimizationOfBinaryRelationsService.getNonstrictRelationFromStrict(initRelation);

        Collection<BinaryElement> elements1 = optimizationOfBinaryRelationsService.getOptimumElement(initRelation, nonstrictRelation , 1);
        Collection<BinaryElement> elements2 = optimizationOfBinaryRelationsService.getOptimumElement(initRelation, nonstrictRelation , 2);
        Collection<BinaryElement> elements3 = optimizationOfBinaryRelationsService.getOptimumElement(initRelation, nonstrictRelation , 3);

        //BinaryRelation additionOperationResult = optimizationOfBinaryRelationsService.additionOperation(initRelation);

        JLabel initialBinaryRelationLabel = new JLabel("Исходное бинарное отношение:");
        examplePanel.add(Box.createRigidArea(new Dimension(750, 10)));
        examplePanel.add(initialBinaryRelationLabel);
        examplePanel.add(Box.createRigidArea(new Dimension(750, 5)));
        examplePanel.add(getJPanelForBinaryRelation(initRelation));

        JLabel additionOperationResultLabel = new JLabel("Дополнение:");
        examplePanel.add(Box.createRigidArea(new Dimension(750, 20)));
        examplePanel.add(additionOperationResultLabel);
        examplePanel.add(Box.createRigidArea(new Dimension(750, 5)));
        examplePanel.add(getJPanelForBinaryRelation(additionOperationResult));

        JLabel revertOperationResultLabel = new JLabel("Обращение:");
        examplePanel.add(Box.createRigidArea(new Dimension(750, 20)));
        examplePanel.add(revertOperationResultLabel);
        examplePanel.add(Box.createRigidArea(new Dimension(750, 5)));
        examplePanel.add(getJPanelForBinaryRelation(revertOperationResult));

        JLabel dualRelationLabel = new JLabel("Двойственное отношение:");
        examplePanel.add(Box.createRigidArea(new Dimension(750, 20)));
        examplePanel.add(dualRelationLabel);
        examplePanel.add(Box.createRigidArea(new Dimension(750, 5)));
        examplePanel.add(getJPanelForBinaryRelation(dualRelation));

        JLabel equivalenceRelationLabel = new JLabel("Отношение эквивалентности (безразличия):");
        examplePanel.add(Box.createRigidArea(new Dimension(750, 20)));
        examplePanel.add(equivalenceRelationLabel);
        examplePanel.add(Box.createRigidArea(new Dimension(750, 5)));
        examplePanel.add(getJPanelForBinaryRelation(equivalenceRelation));

        JLabel nonstrictRelationLabel = new JLabel("Отношение нестрогого порядк:");
        examplePanel.add(Box.createRigidArea(new Dimension(750, 20)));
        examplePanel.add(nonstrictRelationLabel);
        examplePanel.add(Box.createRigidArea(new Dimension(750, 5)));
        examplePanel.add(getJPanelForBinaryRelation(nonstrictRelation));

        JLabel method1Label1 = new JLabel("Свойство 1. Максимальный элемент по отношению Р: " + (elements1.isEmpty() ? "-" : elements1));
        examplePanel.add(Box.createRigidArea(new Dimension(750, 20)));
        examplePanel.add(method1Label1);

        JLabel method1Label2 = new JLabel("Свойство 2. Наибольший элемент по отношению Р: " + (elements2.isEmpty() ? "-" : elements2));
        examplePanel.add(Box.createRigidArea(new Dimension(750, 20)));
        examplePanel.add(method1Label2);

        JLabel method1Label3 = new JLabel("Свойство 3. Максимальный элемент по отношению R: " + (elements3.isEmpty() ? "-" : elements3));
        examplePanel.add(Box.createRigidArea(new Dimension(750, 20)));
        examplePanel.add(method1Label3);

        examplePanel.add(container);
        exampleInitialized = true;
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        return saveValuesListener;
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        return loadValuesListener;
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        return defaultValuesListener;
    }

    public JPanel getJPanelForBinaryRelation(BinaryRelation relation) {

        JPanel panel = new JPanel();
        Graph<String,String> graph = relation.getGraph();

        FRLayout layout = new FRLayout<>(graph);
        layout.setSize(new Dimension(350, 350));
        VisualizationViewer graphVV = new VisualizationViewer<>(layout);
        graphVV.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        graphVV.getRenderer().getVertexLabelRenderer().setPosition(edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position.CNTR);
        graphVV.setSize(new Dimension(350, 350));

        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        Dimension graphDimension = new Dimension(350, 350);
        panel.setMaximumSize(graphDimension);
        panel.setSize(graphDimension);
        panel.setMinimumSize(graphDimension);
        panel.setPreferredSize(graphDimension);
        panel.setBorder(BorderFactory.createEtchedBorder());
        panel.add(graphVV);

        return panel;
    }
}
