package ru.spb.beavers.modules;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import ru.spb.beavers.modules.qualitative_orders.DataModel;

/**
 * Created by laptop on 11.04.2015.
 */
public class QualitativeOrdersModule implements ITaskModule {

    private class SaveListener implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            System.out.println("SaveListener performed");

            //FileNameExtensionFilter filter = new FileNameExtensionFilter("*.*");
            JFileChooser fc = new JFileChooser();
            //fc.setFileFilter(filter);
            if ( fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION ) {
                try ( FileWriter fw = new FileWriter(fc.getSelectedFile()) ) {
                    fw.write(toFile());
                    fw.close();
                }
                catch ( IOException e1 ) {
                    System.out.println("Error of save\n" + e1.toString());
                }
            }

        }

        private String toFile() {
            String s = "";
            for (int i = 0; i < model.getRowCount(); i++) {
                s += model.getValueAt(i, 0) + " " + model.getValueAt(i, 1) + "\n";
            }
            return s;
        }
    }

    private class LoadListener implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            System.out.println("LoadListener performed");

            //FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt", "*.*");
            JFileChooser fc = new JFileChooser();
            //fc.setFileFilter(filter);
            if ( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
                try ( FileReader fr = new FileReader(fc.getSelectedFile()))  {

                    BufferedReader br = new BufferedReader(fr);
                    String line = "";
                    model.eraseData();

                    while ((line = br.readLine()) != null) {
                        if (!line.isEmpty()) {
                            String[] splited = line.split("\\s+");
                            model.appendRow(splited[0], splited[1]);
                        }
                    }
                    fr.close();
                    dataModel.updateUI();
                }
                catch ( IOException e1 ) {
                    System.out.println("Error of load\n" + e1.toString());
                }
            }
        }
    }
    private class DefaultValuesListener implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            System.out.println("DefaultValuesListener performed");
            model.appendRow("10", "5");
            model.appendRow("15", "5");
            model.appendRow("22", "10");
            model.appendRow("39", "22");
            model.appendRow("22", "15");
            dataModel.updateUI();
        }
    }

    private SaveListener saveListener = null;
    private LoadListener loadListener = null;
    private DefaultValuesListener defaultValuesListener = null;
    Boolean antireflect = true;
    Boolean transitiv = true;
    @Override
    public String getTitle() {
        return "Качественные порядки";
    }

    // Текстовое поле для краткого описания решаемой задачи
    private JTextArea descriptionTextArea = new JTextArea(35, 70);

    @Override
    public void initDescriptionPanel(JPanel panel) {
        panel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = GridBagConstraints.RELATIVE;
        constraints.gridy = GridBagConstraints.RELATIVE;
        constraints.fill = GridBagConstraints.BOTH;

        descriptionTextArea.setLineWrap(true);
        descriptionTextArea.setWrapStyleWord(true);
        Font font2 = new Font("Verdana", Font.PLAIN, 12);
        descriptionTextArea.setFont(font2);
        descriptionTextArea.setText("1) ввести набор альтернатив\n" +
                "2) задать на множестве альтернатив бинарное отношение \"качественный порядок\"\n" +
                "3) проверить, действительно ли отношение является отношением качественного порядка\n" +
                "4) реализовать операции над отношениями и применить их к заданному отношению\n" +
                "5) получить отношение безразличия\n" +
                "6) отобразить все полученные данные в виде графов или отрезков");

        panel.add(descriptionTextArea, constraints);
    }

    @Override
    public void initSolutionPanel(JPanel panel) {
        //Get file from resources folder
        ImageIcon icon = new ImageIcon(DataModel.class.getResource("images/mainText.jpg"));
        icon = new ImageIcon(icon.getImage().getScaledInstance(750, icon.getIconHeight(), BufferedImage.SCALE_SMOOTH));

        JLabel htmlLabel = new JLabel();
        htmlLabel.setIcon(icon);
        panel.add(htmlLabel);

    }

    JTextField labelField1 = new JTextField();

    JTextField inputField1 = new JTextField(12);
    JTextField inputField2 = new JTextField(12);

    JTable dataModel = new JTable();
    JButton buttonAdd = new JButton("Добавить");

    DataModel model = new DataModel();

    @Override
    public void initInputPanel(JPanel panel) {

        buttonAdd.setActionCommand("add");

        dataModel = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(dataModel);
        panel.setLayout(new BorderLayout());
        FlowLayout layout2 = new FlowLayout();

        labelField1.setText("Введите отношение (Предпочтительное указывать первым):");
        labelField1.setEnabled(false);
        labelField1.setEditable(false);

        inputField1.setEditable(true);

        JPanel upperPanel = new JPanel(new FlowLayout());


        buttonAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.appendRow(inputField1.getText(), inputField2.getText());
                dataModel.updateUI();
            }
        });

        upperPanel.add(labelField1);
        upperPanel.add(inputField1);
        upperPanel.add(inputField2);
        upperPanel.add(buttonAdd);

        panel.add(upperPanel, BorderLayout.PAGE_START);
        panel.add(scrollPane, BorderLayout.CENTER);

    }
    GraphZoomScrollPane scrollPane;
    JTextArea resultArea = new JTextArea(35, 70);
    JButton buttonI = new JButton("Преобразовать в отношение безразличия");
    @Override
    public void initExamplePanel(JPanel panel) {
        final DirectedSparseMultigraph<String, String> g =
                new DirectedSparseMultigraph<String, String>();
        String edgeName = "";
        for (int i = 0; i < model.getRowCount(); i++) {
            if (!g.containsVertex((String) model.getValueAt(i, 0)))
            {
                g.addVertex((String) model.getValueAt(i, 0));
            }
            if (!g.containsVertex((String) model.getValueAt(i, 1)))
            {
                g.addVertex((String) model.getValueAt(i, 1));
            }
            if (Objects.equals((String) model.getValueAt(i, 1), (String) model.getValueAt(i, 0)))
                antireflect = false;
            edgeName = ((String) model.getValueAt(i, 0)) + "-" + ((String) model.getValueAt(i, 1));
            if(!g.containsEdge(edgeName))
                g.addEdge(edgeName,(String) model.getValueAt(i, 0), (String) model.getValueAt(i, 1));
        }
        System.out.println("antireflect: " + antireflect);
        Integer[][] inMatrix = new Integer[g.getVertexCount()][g.getVertexCount()];
        Integer[][] sMatrix = new Integer[g.getVertexCount()][g.getVertexCount()];
        Map <String, Integer> vertMap = new HashMap<>();
        int v = 0;
        for (String s : g.getVertices()) {
            vertMap.put(s, v);
            v++;
        }

        for (int i = 0; i < g.getVertexCount(); i ++) {
            for (int j = 0; j < g.getVertexCount(); j++) {
                inMatrix[i][j] = 0;
            }
        }
        for (String s : g.getVertices()) {
            for (String s1 : g.getIncidentEdges(s)) {
                if (Objects.equals(g.getEndpoints(s1).getFirst(), s)) {
                    inMatrix[vertMap.get(g.getEndpoints(s1).getFirst())][vertMap.get(g.getEndpoints(s1).getSecond())] = 1;
                }
            }
        }

        for (int i = 0; i < g.getVertexCount(); i ++) {
            for (int j = 0; j < g.getVertexCount(); j++) {
                sMatrix[i][j] = 0;
                for (int k = 0; k < g.getVertexCount(); k++) {
                    sMatrix[i][j] = sMatrix[i][j] | inMatrix[i][k] & inMatrix[k][j];
                }
            }
        }

        for (int i = 0; i < g.getVertexCount(); i ++) {
            for (int j = 0; j < g.getVertexCount(); j++) {
                if (inMatrix[i][j] < sMatrix[i][j]) {
                    transitiv = false;
                }
            }
        }


        FRLayout layout = new FRLayout<>(g);
        final VisualizationViewer/*<yournode, youredge>*/ vv = new VisualizationViewer<>(layout);
        System.out.println("The graph g = " + g.toString());
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        //scrollPane = new GraphZoomScrollPane(vv);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        //panel.add(resultArea);
        JLabel htmlLabel = new JLabel("<html>");
        String buildingString = "<html>Свойство антирефлексивности: ";
        if (antireflect)
            buildingString += "<b>ВЫПОЛНЯЕТСЯ</b><br>\n";
        else
            buildingString += "<b>НЕ ВЫПОЛНЯЕТСЯ</b><br>\n";

        buildingString += "Свойство транзитивности: ";
        if (transitiv)
            buildingString += "<b>ВЫПОЛНЯЕТСЯ</b><br>\n";
        else
            buildingString += "<b>НЕ ВЫПОЛНЯЕТСЯ</b><br>\n";

        buildingString += g.toString() + "<br>";

        buildingString += "Порядок ";
        if (!(antireflect && transitiv))
            buildingString += "<b>НЕ</b> ";

        buildingString += "является <b>качественным</b>";

        buttonI.setActionCommand("gotoi");
        buttonI.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //сделаем симметричным
                String first = "";
                String second = "";

                Collection<String> vertex = new ArrayList(g.getVertices());
                for (String i : vertex) {
                    for (String j : vertex) {
                        if (g.containsEdge(i+"-"+j))
                            for (String k : vertex) {
                                if (g.containsEdge(j+"-"+k))
                                    g.addEdge(i+"-"+k, i, k);
                            }
                    }
                }

                Collection<String> edges = new ArrayList(g.getEdges());
                for (String s : edges) {
                    first = g.getEndpoints(s).getFirst();
                    second = g.getEndpoints(s).getSecond();
                    if (!g.containsEdge(first+"-"+first)) {
                        g.addEdge(first+"-"+first, first, first);
                    }
                    if (!g.containsEdge(second+"-"+second)) {
                        g.addEdge(second+"-"+second, second, second);
                    }
                    if (!g.containsEdge(second+"-"+first)) {
                        g.addEdge(second+"-"+first, second, first);
                    }
                }
                vv.updateUI();
            }
        });

        htmlLabel.setText(buildingString);
        htmlLabel.setVerticalAlignment(SwingConstants.CENTER);
        htmlLabel.setHorizontalAlignment(SwingConstants.CENTER);
        buttonI.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(htmlLabel);
        panel.add(buttonI);
        panel.add(vv);
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        if (saveListener == null) {
            saveListener = new SaveListener();
        }
        return saveListener;
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        if (loadListener == null) {
            loadListener = new LoadListener();
        }
        return loadListener;
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        if (defaultValuesListener == null) {
            defaultValuesListener = new DefaultValuesListener();
        }
        return defaultValuesListener;
    }
}
