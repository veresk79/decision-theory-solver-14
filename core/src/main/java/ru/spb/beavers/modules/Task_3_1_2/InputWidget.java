package ru.spb.beavers.modules.Task_3_1_2;

import com.sun.javafx.beans.annotations.NonNull;
import ru.spb.beavers.modules.Task_3_1_1.attachCapable;
import ru.spb.beavers.modules.Task_3_1_1.creatComponentAsist;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Никита on 12.04.15.
 */
public class InputWidget implements attachCapable {

    private JPanel  MAIN_PANEL = new JPanel();;
    private JTextField PlaceNumField=new JTextField(12);
    private JTextField SprosValueField=new JTextField(12);
    private JTextField PriceVfField=new JTextField(12);
    private JTextField PriceVdField=new JTextField(12);
    private JTextField VerField=new JTextField(12);

    private JRadioButton buttonClass1= new JRadioButton(TaskCommon.SOLUTION_CLASS_1);
    private JRadioButton buttonClass2= new JRadioButton(TaskCommon.SOLUTION_CLASS_2);

    private short SolutionType = 1;
    private int PlaceNum = 0; /** количество мест M **/
    private int PriceVf = 0; /** цена 1 **/
    private int PriceVd = 0; /** цена 2 **/
    private double VerP = 0.5;

    private boolean flagActive = false;

    public InputWidget()
    {
        MAIN_PANEL.setLayout(new BoxLayout(MAIN_PANEL, BoxLayout.Y_AXIS));
        defaultParam();
        initTextFields();
        buttonClass1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SolutionType = 1;
                flagActive = false;
                VerField.setEnabled(false);
            }
        });
        buttonClass2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SolutionType = 2;
                flagActive = true;
                VerField.setEnabled(true);

            }
        });
        ButtonGroup group = new ButtonGroup();
        group.add(buttonClass1);
        group.add(buttonClass2);
        JLabel Title = new JLabel("<html><h2>Данные для ввода</h2></html>");
        MAIN_PANEL.add(new JPanel().add(Title));
        MAIN_PANEL.add(creatComponentAsist.customPanel(TaskCommon.SOLUTION_TYPE, buttonClass1, buttonClass2));
        MAIN_PANEL.add(creatComponentAsist.customPanel(TaskCommon.COUNT_PLACE, PlaceNumField));
        MAIN_PANEL.add(creatComponentAsist.customPanel(TaskCommon.PRICE_CLASS_1, PriceVfField));
        MAIN_PANEL.add(creatComponentAsist.customPanel(TaskCommon.PRICE_CLASS_2, PriceVdField));
        MAIN_PANEL.add(creatComponentAsist.customPanel(TaskCommon.VEROYATNOST, VerField));
    }

    @Override
    public void attachToExPanel(@NonNull JPanel exPanel)
    {
        exPanel.removeAll();
        exPanel.add(MAIN_PANEL);
    }

    private void initTextFields()
    {

        PlaceNumField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("Text PN=" + PlaceNumField.getText());
                try{
                    PlaceNum = Integer.parseInt(PlaceNumField.getText());
                }
                catch(NumberFormatException ex)
                {
                    PlaceNum = 100;
                    JOptionPane.showMessageDialog(null, "Ошибка при вводе количества мест!");
                    ex.printStackTrace();
                }

            }
        });

        PriceVfField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("Text Vf=" + PriceVfField.getText());
                try{
                    PriceVf = Integer.parseInt(PriceVfField.getText());
                }
                catch(NumberFormatException ex)
                {
                    PriceVf = 1400;
                    JOptionPane.showMessageDialog(null, "Ошибка при вводе Vf!");
                    ex.printStackTrace();
                }
            }
        });

        PriceVdField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("Text Vd=" + PriceVdField.getText());
                try{
                    PriceVd = Integer.parseInt(PriceVdField.getText());
                }
                catch(NumberFormatException ex)
                {
                    PriceVd = 700;
                    JOptionPane.showMessageDialog(null, "Ошибка при вводе Vd!");
                    ex.printStackTrace();
                }
                if(PriceVf <= PriceVd)
                    JOptionPane.showMessageDialog(null, "Стоимость билета со скидкой должна быть меньше стоимости билетов без скидки");
            }
        });


    }

    public void setDefaultFieldValues()
    {
        buttonClass1.setSelected(true);
        SolutionType = 1;
        PlaceNumField.setText("100");
        PriceVfField.setText("1400");
        PriceVdField.setText("700");
        VerField.setEnabled(false);

    }


    public String toString()
    {
        String asString = ""+ SolutionType +"\n"+
                           PlaceNum+"\n"+
                           PriceVf + "\n" +
                           PriceVd + "\n" +
                           VerP;
        return  asString;
    }

    public void loadData(String dataString)
    {
        Pattern pattern = Pattern.compile("((\\\"[^\"]+\\\")|\\S+)");
        Matcher matcher = pattern.matcher(dataString);
        for(int i=0; i<5 && matcher.find(); i++)
        {
            try{
                switch (i)
                {
                    case 0:
                    {

                            if(Integer.parseInt(matcher.group())==1)
                            {
                                buttonClass1.setSelected(true);
                                SolutionType =1;

                            }
                            else
                            {
                                buttonClass2.setSelected(true);
                                SolutionType =2;
                            }

                    }
                    break;

                    case 1:
                    {
                        PlaceNumField.setText(matcher.group());

                    }
                    break;
                    case 2:
                    {
                        PriceVfField.setText(matcher.group());

                    }
                    break;
                    case 3:
                    {
                        PriceVdField.setText(matcher.group());

                    }
                    break;
                    case 4:
                    {
                        VerField.setText(matcher.group());

                    }
                    break;

                }
            }
            catch (Exception e)
            {
                System.out.println("LOG:: load file exception");
                e.printStackTrace();
            }
        }

}

    private void defaultParam()
    {
        SolutionType = 2; // тип спроса
        PlaceNum = 100; /** количество мест M **/
        PriceVf = 1400; /** цена 1 **/
        PriceVd = 700; /** цена 2 **/
        VerP = 0;
    }

    public short getSolutionType()
    {
        return SolutionType;
    }

    public int getPlaceNum()
    {
        try{
            PlaceNum = Integer.parseInt(PlaceNumField.getText());
            if(checkMinus(PlaceNum)) throw new Exception("Ниже нуля");
        }
        catch (Exception ex)
        {
            printAlert("Количество мест");
            ex.printStackTrace();
            PlaceNum = 100;
        }
        return PlaceNum;
    }

    public int getPriceVf()
    {
        try{
            PriceVf = Integer.parseInt(PriceVfField.getText());
            if(checkMinus(PriceVf)) throw new Exception("Ниже нуля");
        }
        catch (Exception ex)
        {
            printAlert("Цена без скидки");
            ex.printStackTrace();
            PriceVf = 1400;
        }
        return PriceVf;
    }
    public int getPriceVd()
    {
        try{
            PriceVd = Integer.parseInt(PriceVdField.getText());
            if(checkMinus(PriceVd)) throw new Exception("Ниже нуля");
        }
        catch (Exception ex)
        {
            printAlert("Цена со скидкой");
            ex.printStackTrace();
            PriceVd = 700;
        }
        return PriceVd;
    }

    public double getVerP()
    {
        try{
            if(VerField.isEnabled()) {
                VerP = Double.parseDouble(VerField.getText());
                if(checkMinus(VerP)) throw new Exception("Ниже нуля");
            }
            else return 0;
        }
        catch (Exception ex)
        {
            printAlert("Вероятность");
            ex.printStackTrace();
            VerP = 0.5;
        }
        return VerP;
    }

    public void printAlert(String fieldName)
    {
        JOptionPane.showMessageDialog(null, "Ошибка при вводе значения; поле "+fieldName+". Значение установлено по умолчанию");
    }

    public boolean checkMinus(double value)
    {
        if(value<0) return true;
        else return  false;
    }
}
