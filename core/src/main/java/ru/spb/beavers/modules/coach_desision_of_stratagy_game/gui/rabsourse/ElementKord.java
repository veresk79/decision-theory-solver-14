package ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse;

/**
 * Created by Sasha on 09.04.2015.
 */
public class ElementKord {
    public int y;
    public int height;

    public ElementKord()
    {
        this.y = 0;
        this.height = 0;
    }

    public ElementKord(int y, int height)
    {
        this.y = y;
        this.height = height;
    }

    public void updateValues(int oy, int height)
    {
        this.y = this.y + this.height + oy;
        this.height = height;
    }

}
