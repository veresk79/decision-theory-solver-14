package ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui;


/**
 * Created by Sasha on 09.04.2015.
 */



import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import info.monitorenter.gui.chart.views.ChartPanel;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.PanelEl;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.GroupableTableHeader;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.ElementKord;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.util.Map;
import java.util.Vector;

public class SolutionPanel {
    private static Map vx_coords;
    private static Map edge_values;

    private ElementKord last_el_coords;

    public SolutionPanel(JPanel panel)
    {
        last_el_coords = new ElementKord();

        panel.setLayout(null);
        panel.setSize(760, 4700);
        panel.setPreferredSize(new Dimension(750, 4700));


        PanelEl.setTextHeader("Термины американского футбола", 20, 300, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 400);
        PanelEl.setHtmlText600Px("./ru/spb/beavers/modules/coach_desision_of_stratagy_game/Termin_American_Football.html", last_el_coords, panel);


        PanelEl.setTextHeader("Теоретический расчет полезностей", 30, 400, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 400);
        PanelEl.setHtmlText600Px("./ru/spb/beavers/modules/coach_desision_of_stratagy_game/Formul_solush.html", last_el_coords, panel);

        // Расчет полезностей решений.
        PanelEl.setTextHeader("Пример расчета полезностей решений", 30, 400, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 380);
        PanelEl.setHtmlText600Px("./ru/spb/beavers/modules/coach_desision_of_stratagy_game/number_solution.html", last_el_coords, panel);

        // Дерево решений
        createDecisionTree(panel);
        //



        // Таблица 1.4
        createCalcTable(panel);
        // График зависимости Vd от p
        createPlotPW(panel);
        // «(Текст после графика)
        last_el_coords.updateValues(30, 150);
        PanelEl.setHtmlText600Px("./ru/spb/beavers/modules/coach_desision_of_stratagy_game/Text_graf2.html", last_el_coords, panel);

        createPlotPW2(panel);
        // «(Текст после графика)
        last_el_coords.updateValues(30, 150);
        PanelEl.setHtmlText600Px("./ru/spb/beavers/modules/coach_desision_of_stratagy_game/Text_graf1.html", last_el_coords, panel);


    }

    public void createCalcTable(JPanel panel)
    {
        DefaultTableModel dm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        dm.setDataVector(new Object[][]{
                        {"0.1", "12.6", "13"},
                        {"0.15", "12.9", "13.5"},
                        {"0.2", "13.2", "14"},
                        {"0.25", "13.5", "14.5"},
                        {"0.3", "13.8", "15"},
                        {"0.35", "14.1", "15.5"},
                        {"0.4", "14.4", "16"},
                        {"0.45", "14.7", "16.5"},
                        {"0.5", "15", "17"},
                        {"0.55", "15.3", "17.5"},
                        {"0.6", "15.6", "18"},
                        {"0.65", "15.9", "18.5"},
                        {"0.7", "16.2", "19"},
                        {"0.75", "16.5", "19.5"},
                        {"0.8", "16.8", "20"},
                        {"0.85", "17.1", "20.5"},
                        {"0.9", "17.4", "21", ""},
                        {"0.95", "17.7", "21.5"},
                        {"1", "18", "22"},

                },
                new Object[]{"Вероятность\nполучить очки", "Ценночть решения\nзабить гол", "Ценность решения\nзабить тачдаун"});
        JTable table = new JTable( dm ) {
            DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

            {
                renderRight.setHorizontalAlignment(SwingConstants.CENTER);
            }

            @Override
            public TableCellRenderer getCellRenderer (int arg0, int arg1) {
                return renderRight;
            }

            protected JTableHeader createDefaultTableHeader() {
                return new GroupableTableHeader(columnModel);
            }
        };
        TableColumnModel cm = table.getColumnModel();
        cm.getColumn(0).setMaxWidth(300);
        cm.getColumn(1).setMaxWidth(300);
        cm.getColumn(2).setMaxWidth(300);
        table.setPreferredSize(new Dimension(650,table.getRowHeight()*19));
        table.setSize(650, table.getRowHeight() * 19);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setSize(650, 467);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(30, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - table.getWidth() / 2, last_el_coords.y);
        panel.add(scroll);
    }

    public void createPlotPW(JPanel panel)
    {


        Chart2D chart = new Chart2D();
        chart.getAxisX().getAxisTitle().setTitle("P");
        chart.getAxisY().getAxisTitle().setTitle("U(d1)");
        ITrace2D trace = new Trace2DSimple();
        trace.setName("");
        chart.addTrace(trace);
        double Ud = 0;
        double P = 0;
        int Preit=22;
        int Nreit=18;
        int prreit=12;

        trace.addPoint(0, 0);
        do {
            Ud = P*Nreit+(1-P)*prreit;
            /*if (Ud > 0.01) {
                trace.addPoint(P, Ud);
            }*/trace.addPoint(P, Ud);

            P=P+0.01;
        } while((P > 0.0)&&(P<=1));
        ChartPanel cp = new ChartPanel(chart);
        cp.setSize(600, 400);
        last_el_coords.updateValues(30, cp.getHeight());
        cp.setLocation(panel.getWidth() / 2 - cp.getWidth() / 2, last_el_coords.y);

        panel.add(cp);

    }


    public void createPlotPW2(JPanel panel)
    {


        Chart2D chart = new Chart2D();
        chart.getAxisX().getAxisTitle().setTitle("P");
        chart.getAxisY().getAxisTitle().setTitle("U(d1)");
        ITrace2D trace = new Trace2DSimple();
        trace.setName("");
        chart.addTrace(trace);
        double Ud = 0;
        double P = 0;
        int Preit=22;
        int Nreit=18;
        int prreit=12;

        trace.addPoint(0, 0);
        do {
            Ud = P*Preit+(1-P)*prreit;
           trace.addPoint(P, Ud);

            P=P+0.01;
        } while((P > 0.0)&&(P<=1));
        ChartPanel cp = new ChartPanel(chart);
        cp.setSize(600, 400);
        last_el_coords.updateValues(60, cp.getHeight());
        cp.setLocation(panel.getWidth() / 2 - cp.getWidth() / 2, last_el_coords.y);

        panel.add(cp);

    }

    public void createDecisionTree(JPanel panel)
    {
        Vector<String> vector = new Vector();
        vector.add(" "); // N
        vector.add(" "); // T
        vector.add(" "); // C1
        vector.add(" "); // s1
        vector.add(" "); // f1
        vector.add(" "); // p
        vector.add(" "); // C2
        vector.add(" "); // s2
        vector.add(" "); // f2
        vector.add(" "); // A1
        vector.add(" "); // s3
        vector.add(" "); // m1
        vector.add(" "); // l1
        vector.add(" "); // n
        vector.add(" "); // C3
        vector.add(" "); // s4
        vector.add(" "); // f3
        vector.add(" "); // A2
        vector.add(" "); // s5
        vector.add(" "); // m2
        vector.add(" "); // l2
        vector.add(" "); // l2
        PanelEl.setDescriptionTree(panel, vector, last_el_coords, 10);
    }
}