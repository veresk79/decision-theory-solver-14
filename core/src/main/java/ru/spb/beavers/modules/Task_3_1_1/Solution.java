package ru.spb.beavers.modules.Task_3_1_1;

import com.sun.istack.internal.Nullable;
import com.sun.javafx.beans.annotations.NonNull;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Никита on 10.04.15.
 */
public class Solution implements  attachCapable{

    private JButton buttonFirstPage = new JButton(TaskCommon.FORMAL_TITLE);
    private JButton buttonSecondPage = new JButton(TaskCommon.USEFULL_TITLE);
    private JButton buttonThirdPage = new JButton(TaskCommon.SOLUTION_TITLE);

    private JPanel  MAIN_PANEL = new JPanel();;
    private JPanel  buttonPanel = new JPanel();
    private JPanel  viewPanel = new JPanel(); ;
    private JPanel  panel1 =  new JPanel();
    private JPanel  panel2 =  new JPanel();
    private JPanel  panel3 =  new JPanel();

    public Solution()
    {
        initPanels();
        buttonFirstPage.addActionListener(AddListener(panel1));
        buttonSecondPage.addActionListener(AddListener(panel2));
        buttonThirdPage.addActionListener(AddListener(panel3));
    }

    private void initButtonPanel()
    {
        buttonPanel.removeAll();
        buttonPanel.add(buttonFirstPage);
        buttonPanel.add(buttonSecondPage);
        buttonPanel.add(buttonThirdPage);
    }

    private void initViewPanel()
    {
        viewPanel.setLayout(new BoxLayout(viewPanel, BoxLayout.Y_AXIS));
        viewPanel.removeAll();
        creatComponentAsist.setPicture(panel1, TaskCommon.PIC_FORMAL);
        creatComponentAsist.setPicture(panel2, TaskCommon.PIC_USEFULL);
        creatComponentAsist.setPicture(panel3, TaskCommon.PIC_SOLUTION);
        viewPanel.add(panel1);
    }

    private void initPanels()
    {
        MAIN_PANEL.setLayout(new BoxLayout(MAIN_PANEL, BoxLayout.Y_AXIS));
        MAIN_PANEL.removeAll();
        initButtonPanel();
        initViewPanel();
        MAIN_PANEL.add(buttonPanel);
        MAIN_PANEL.add(viewPanel);

    }

    private ActionListener AddListener(@NonNull JPanel panelBut)
    {
        final JPanel panelAction = panelBut;
        return new ActionListener() {

            public void actionPerformed(ActionEvent e)
            {
                viewPanel.removeAll();
                viewPanel.add(panelAction);
                viewPanel.revalidate();
                viewPanel.repaint();
                System.out.println("LOG:: Solution - button was clicked");
            }
        };
    }

    @Override
    public void attachToExPanel(@NonNull JPanel exPanel)
    {
        exPanel.removeAll();
        exPanel.add(MAIN_PANEL);
    }


}
