package ru.spb.beavers.modules.battery_task;

/**
 * Клас является хранилищем параметров решаемой задачи
 * и предоставляет к ним глобальную точку доступа
 *
 * @author Alexey Borisov
 */
public class TaskParms {

	public float batteriesCost;
	public float batteriesPrice;
	public float p0;
	public float p1;
	public float p2;
	public float p3;

}