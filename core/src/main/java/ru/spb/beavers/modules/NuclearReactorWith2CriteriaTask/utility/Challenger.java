package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility;

public class Challenger {
    private Node node_;
    private Value value_;

    public Challenger (Node node, Value value) {
        node_ = node;
        value_ = value;
    }

    public Node getNode () {
        return node_;
    }

    public Value getValue () {
        return value_;
    }
}
