\begin{align*}
&\left [ v_{i}\left(\textbf{h}_{i}\right ),u_{i}\left(\textbf{h}_{i}\right ) \right ]= \sum_{j}p_{j}\left(\textbf{h}_{i}\right )\left[ r_{j}\left(\textbf{h}_{i}\right ) + \textit{v}_{k}\left(\textbf{h}_{k}\right ), \textit{s}_{j}\left(\textbf{h}_{i}\right ) + \textit{u}_{k}\left(\textbf{h}_{k}\right ) \right],\\
&i\in C, j\in C\left ( i|\textbf{h}_{i} \right )
\end{align*}