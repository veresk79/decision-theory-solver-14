\begin{align*}
&v_{i}(\textbf{h}_{i})=max_{j\in D(i|\textbf{h}_{i})} \left \{ v_{k}(\textbf{h}_{k}) \right \}\\
\end{align*}