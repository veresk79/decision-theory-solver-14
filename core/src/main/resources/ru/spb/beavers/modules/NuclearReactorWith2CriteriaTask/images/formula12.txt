\begin{align*}
&w\left (r\right )=\frac{\beta }{\beta - 1}\left ( 1 - \beta^{-\left ( \frac{r-r_{min}}{r_{max}-r_{min}} \right )}  \right )\\
\end{align*}