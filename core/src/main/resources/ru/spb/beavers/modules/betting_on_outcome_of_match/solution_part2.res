<html> 
	<p>     
		<b>Рассчет полезностей решений.</b><br/>    

		Ценности исходов определяются с учетом введенных обозначений как: <br/><br/>
		V(y<sub>1</sub>, d<sub>2</sub>) = v(a<sub>1</sub>) = v<sub>1</sub> = 0; <br/>
		V(y<sub>1</sub>, d<sub>1</sub>, x<sub>1</sub>) = v(a<sub>2</sub>) = v<sub>2</sub> = 0.25; <br/>
		V(y<sub>1</sub>, d<sub>1</sub>, x<sub>2</sub>) = v(a<sub>3</sub>) = v<sub>3</sub> = -1; <br/>
		V(y<sub>2</sub>, d<sub>2</sub>) = v(a<sub>4</sub>) = v<sub>4</sub> = 0; <br/>
		V(d<sub>2</sub>, d<sub>1</sub>, x<sub>1</sub>) = v(a<sub>5</sub>) = v<sub>5</sub> = 2. <br/>
		V(d<sub>2</sub>, d<sub>1</sub>, x<sub>2</sub>) = v(a<sub>6</sub>) = v<sub>6</sub> = -1. <br/><br/>

		При Y = y<sub>1</sub>: 
		u(d<sub>1</sub>) = p<sub>11</sub>v<sub>2</sub> + p<sub>21</sub>v<sub>3</sub> = 
			p<sub>11</sub>v<sub>2</sub> + (1 - p<sub>11</sub>)v<sub>3</sub>) = 
			(v<sub>2</sub> - v<sub>3</sub>)p<sub>11</sub> + v<sub>3</sub> = 1.25
			p<sub>11</sub> - 1;
		u(d<sub>2</sub>) = v<sub>1</sub> = 0.<br/>

		При Y = y<sub>2</sub>: 
		u(d<sub>1</sub>) = p<sub>12</sub>v<sub>5</sub> + p<sub>22</sub>v<sub>6</sub> = 
			3p<sub>12</sub> - 1;
		u(d<sub>2</sub>) = v<sub>4</sub> = 0.<br/><br/>
	</p>   


	<b><i>Вариант задачи с учетом дополнительной информации.</i></b><br/> 
	<p>
		Множество решений Нэнси <b>D</b> = {d<sub>1</sub>, d<sub>2</sub>}, 
		случайные величины <b>Y</b> = {y<sub>1</sub>, y<sub>2</sub>} и <b>X</b> = {x<sub>1</sub>, x<sub>2</sub>} 
		остаются неизменными. 

		Но в отличие от предыдущего случая, Нэнси должна принять свое решение до того, как станет известно решение тренера.
		Поэтому она должна оценить вероятность того, какое решение примет тренер: попытаться забить гол или
		попробовать совершить тачдаун. Предполагается, что ее решение будет состоять в том, чтобы заключитьпари только в том случае,
		если ожидаемый доход будет неотрицательным. <br/><br/>

		Обозначим p<sub>k</sub> = Pr{Y = y<sub>1</sub>} – вероятность того, что тренер примет решение попытаться забить гол. 
		В зависимости от принятого решения Нэнси, тренера и его результата, выделим пять исходов (альтернатив), входящих
		во множество исходов (альтернатив) <b>A</b> = {a<sub>1</sub>, a<sub>2</sub>, a<sub>3</sub>, a<sub>4</sub>, a<sub>5</sub>}, где <br/>
		a<sub>1</sub> – Нэнси заключила пари, тренер принял решение попытаться забить гол, команда выиграла; 
		a<sub>2</sub> – Нэнси заключила пари, тренер принял решение попытаться забить гол, команда проиграла;
		a<sub>3</sub> – Нэнси заключила пари, тренер принял решение попытаться совершить тачдаун, команда выиграла;<br/> 
		a<sub>4</sub> – Нэнси заключила пари, тренер принял решение попытаться совершить тачдаун, команда проиграла; 
		a<sub>5</sub> – Нэнси решила не заключать пари. <br/><br/>  

		<b>Дерево решений для данной задачи будет иметь следующий вид:</b><br/><br/>

	</p> 
</html>